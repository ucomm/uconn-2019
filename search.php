<?php
    // https://github.com/BoldGrid/w3-total-cache/wiki/FAQ:-Usage#which-textareas-for-file-entries-support-regular-expressions
    if (!defined('DONOTCACHEPAGE')) {
        define('DONOTCACHEPAGE', true);
    }
    add_filter('body_class', array('UConn2019\Lib\Helpers', 'add_angled_header_class'));
    get_header(); 
    $searchResults = $wp_query->found_posts;
    $hasResults = $searchResults > 0;
    if (!isset($helpers) || !class_exists('UConn2019\Lib\Helpers')) {
        include UCONN_2019_DIR . '/lib/Helpers.php';
        $helpers = new \UConn2019\Lib\Helpers();
    }
?>

    <main role="main" id="main-content">
        <?php echo $helpers->get_angled_header('Search'); ?>
        <div class="search-contain">
            <section id="search-wrapper">
                <h2><?php 
                    if (!$hasResults) {
                        echo "No Results Found.";
                    }
                    else {
                        echo sprintf( __( '%s Search Results Found', 'uconn-2019' ), $wp_query->found_posts );
                    } ?>
                </h2>
                <?php get_template_part( 'template-parts/content', 'search' ); ?>
            </section>
        </div>
    </main>

<?php get_footer(); ?>