<?php

/**
 * 
 * Template Name: Angled Header
 * 
 * Creates a template for non-BB pages that also need an angled header
 * 
 */
    add_filter('body_class', array('UConn2019\Lib\Helpers', 'add_angled_header_class'));
    get_header();

    if (!isset($helpers) || !class_exists('UConn2019\Lib\Helpers')) {
        include UCONN_2019_DIR . '/lib/Helpers.php';
        $helpers = new \UConn2019\Lib\Helpers();
    }

?>

    <main role="main" aria-label="Content" id="main-content">
        <?php 
            $title = get_the_title();
            echo $helpers->get_angled_header($title); 
        ?>
        <section>
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <?php the_content(); ?>
                    <?php edit_post_link(); ?>
                </article>
            <?php endwhile; ?>

            <?php else : ?>
                <?php get_template_part('template-parts/content', 'none'); ?>
            <?php endif; ?>
        </section>
    </main>


<?php get_footer(); ?>