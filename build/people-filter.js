/******/ (function() { // webpackBootstrap
/******/ 	"use strict";
/*!*********************************!*\
  !*** ./src/js/people-filter.ts ***!
  \*********************************/

const personHideClass = 'person-info-hide';
const peopleGrid = document.getElementById('people-grid');
const groupSelect = document.getElementById('group-select');
const personTagSelect = document.getElementById('persontag-select');
const nameSelect = document.getElementById('name-select');
const peopleInfo = document.querySelectorAll('.person-info');
document.addEventListener('DOMContentLoaded', () => {
    const selectChangeHandler = (target, type) => {
        const { value } = target;
        const dataAtt = `data-${type}`;
        const hideClass = `${personHideClass}-${type}`;
        peopleInfo.forEach(personEl => {
            const groups = personEl.getAttribute(dataAtt);
            const hasGroupVal = groups?.includes(value);
            if (value === 'all-groups' || value === 'all-persontags') {
                personEl.classList.remove(hideClass);
                return;
            }
            if (!hasGroupVal && !personEl.classList.contains(hideClass)) {
                personEl.classList.add(hideClass);
            }
            else if (hasGroupVal && personEl.classList.contains(hideClass)) {
                personEl.classList.remove(hideClass);
            }
        });
    };
    groupSelect.addEventListener('change', ({ target }) => {
        const selectTarget = target;
        selectChangeHandler(selectTarget, 'groups');
    });
    personTagSelect.addEventListener('change', ({ target }) => {
        const selectTarget = target;
        selectChangeHandler(selectTarget, 'persontags');
    });
    nameSelect.addEventListener('keyup', (evt) => {
        const target = evt.target;
        peopleInfo.forEach(personEl => {
            const personName = personEl.querySelector('.person-name');
            const personNameText = personName.innerText.toLowerCase();
            const matchesName = personNameText.includes(target.value.toLowerCase());
            if (!matchesName && !personEl.classList.contains(personHideClass)) {
                personEl.classList.add(personHideClass);
            }
            else if (matchesName && personEl.classList.contains(personHideClass)) {
                personEl.classList.remove(personHideClass);
            }
        });
    });
});
document.addEventListener('keyup', ({ code }) => {
    if (code === 'Escape') {
        nameSelect.value = '';
        groupSelect.selectedIndex = 0;
        personTagSelect.selectedIndex = 0;
        peopleInfo.forEach(personEl => {
            personEl.className = 'person-info';
        });
    }
});

/******/ })()
;
//# sourceMappingURL=people-filter.js.map