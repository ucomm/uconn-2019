/******/ (function() { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./src/admin/editor/admin-variables.scss":
/*!***********************************************!*\
  !*** ./src/admin/editor/admin-variables.scss ***!
  \***********************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
// extracted by mini-css-extract-plugin


/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	!function() {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = function(exports) {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	}();
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
/*!************************************!*\
  !*** ./src/admin/editor/editor.ts ***!
  \************************************/
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _admin_variables_scss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./admin-variables.scss */ "./src/admin/editor/admin-variables.scss");
// @ts-nocheck

// initialize
let pageTemplate = null;
const listenForPageTemplate = () => {
    // subscribe to changes in the editor's state
    wp.data.subscribe(() => {
        // when the state changes, get the page template
        const newPageTemplate = wp.data.select('core/editor').getEditedPostAttribute('template');
        // make sure that the correct value is read when the editor first loads
        // then continue updating whenever the template changes
        if ((newPageTemplate !== undefined && pageTemplate === null) ||
            (newPageTemplate !== undefined && newPageTemplate !== pageTemplate)) {
            pageTemplate = newPageTemplate;
            pageTemplate.includes('angled-header') ?
                document.body.classList.add('angled-header-template') :
                document.body.classList.remove('angled-header-template');
        }
    });
};
listenForPageTemplate();

/******/ })()
;
//# sourceMappingURL=editor.js.map