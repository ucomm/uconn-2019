/******/ (function() { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./src/sass/main.scss":
/*!****************************!*\
  !*** ./src/sass/main.scss ***!
  \****************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
// extracted by mini-css-extract-plugin


/***/ }),

/***/ "./src/js/az-search.ts":
/*!*****************************!*\
  !*** ./src/js/az-search.ts ***!
  \*****************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
const toggleClass = (el, isHidden) => isHidden ?
    el.classList.add('hidden') :
    el.classList.remove('hidden');
const resultsTextHandler = (el, count) => {
    if (!el) {
        return;
    }
    const countSpan = el.querySelector('.count');
    const pluralSpan = el.querySelector('.is-plural');
    if (count >= 0) {
        el.style.visibility = 'visible';
        countSpan.innerText = `${count}`;
        pluralSpan.innerText = count === 1 ? '' : 's';
    }
    else {
        el.style.visibility = 'hidden';
        countSpan.innerText = '0';
        pluralSpan.innerText = 's';
    }
};
const azSearch = () => {
    const input = document.querySelector('#az-search');
    if (!input) {
        return;
    }
    const containers = Array.from(document.querySelectorAll('.az-container'));
    const items = Array.from(document.querySelectorAll('.az-link-list li'));
    const resultsText = document.querySelector('#results-text');
    const count = items.length;
    input.addEventListener('keyup', (evt) => {
        const target = evt.target;
        if (evt.code === 'Escape') {
            /**
             *
             * if the esc key is pressed while the mobile menu is available,
             * the key press should not trigger the menu to open.
             * stopImmediatePropagation prevents the event from bubbling beyond the input
             *
             */
            evt.stopImmediatePropagation();
            target.value = '';
        }
        if (target.value.length > 1) {
            const searchTerm = target.value.trim().toLowerCase();
            // hide items that don't match the search term
            const filteredItems = items.filter(item => {
                toggleClass(item, false);
                if (!item.innerText.toLowerCase().includes(searchTerm)) {
                    toggleClass(item, true);
                    return item;
                }
            });
            // manage list containers
            containers.map(container => {
                const items = container.querySelectorAll('li');
                const hiddenItems = container.querySelectorAll('li.hidden');
                const shownItems = container.querySelectorAll('li:not(.hidden)');
                // if there's only one item, force it to the left of the page
                if (shownItems.length === 1) {
                    shownItems[0].style.columnSpan = 'all';
                }
                // check to see if there are any items still visible in the container
                // if not, hide the container
                items.length === hiddenItems.length ?
                    toggleClass(container, true) :
                    toggleClass(container, false);
            });
            // show how many results there are
            resultsTextHandler(resultsText, count - filteredItems.length);
        }
        else {
            // hide the results text and show all items
            resultsTextHandler(resultsText, -1);
            items.map(i => toggleClass(i, false));
            containers.map(c => toggleClass(c, false));
        }
    });
};
/* harmony default export */ __webpack_exports__["default"] = (azSearch);


/***/ }),

/***/ "./src/js/mobilenav.ts":
/*!*****************************!*\
  !*** ./src/js/mobilenav.ts ***!
  \*****************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
const toggleMobileMenu = (evt) => {
    const type = evt.type;
    const activeNav = document.querySelector('.mobile-navigation-container');
    switch (type) {
        case 'click':
            activeNav.classList.toggle('active');
            break;
        case 'keyup':
            const { code } = evt;
            if (code === 'Escape' && activeNav) {
                activeNav.classList.toggle('active');
            }
            else {
                return;
            }
            break;
        default:
            return;
    }
};
const bindMobileToggle = () => {
    const mobileTrigger = document.querySelector('.mobile-trigger');
    const mobileClose = document.querySelector('.mobile-navigation-close');
    mobileTrigger.addEventListener('click', toggleMobileMenu);
    mobileClose.addEventListener('click', toggleMobileMenu);
    document.addEventListener('keyup', toggleMobileMenu);
};
/* harmony default export */ __webpack_exports__["default"] = (bindMobileToggle);


/***/ }),

/***/ "navigation":
/*!*****************************!*\
  !*** external "Navigation" ***!
  \*****************************/
/***/ (function(module) {

module.exports = Navigation;

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/compat get default export */
/******/ 	!function() {
/******/ 		// getDefaultExport function for compatibility with non-harmony modules
/******/ 		__webpack_require__.n = function(module) {
/******/ 			var getter = module && module.__esModule ?
/******/ 				function() { return module['default']; } :
/******/ 				function() { return module; };
/******/ 			__webpack_require__.d(getter, { a: getter });
/******/ 			return getter;
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/define property getters */
/******/ 	!function() {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = function(exports, definition) {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	!function() {
/******/ 		__webpack_require__.o = function(obj, prop) { return Object.prototype.hasOwnProperty.call(obj, prop); }
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	!function() {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = function(exports) {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	}();
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
/*!*************************!*\
  !*** ./src/js/index.ts ***!
  \*************************/
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var navigation__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! navigation */ "navigation");
/* harmony import */ var navigation__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(navigation__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _mobilenav__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./mobilenav */ "./src/js/mobilenav.ts");
/* harmony import */ var _az_search__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./az-search */ "./src/js/az-search.ts");
/* harmony import */ var _sass_main_scss__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../sass/main.scss */ "./src/sass/main.scss");
// @ts-ignore




document.addEventListener('DOMContentLoaded', () => {
    // Add support for the A11y Menu
    const navigation = new (navigation__WEBPACK_IMPORTED_MODULE_0___default())({
        'menuId': 'am-main-menu',
        'click': true
    });
    navigation.init();
    (0,_mobilenav__WEBPACK_IMPORTED_MODULE_1__["default"])();
    (0,_az_search__WEBPACK_IMPORTED_MODULE_2__["default"])();
});

/******/ })()
;
//# sourceMappingURL=index.js.map