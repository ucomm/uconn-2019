/******/ (function() { // webpackBootstrap
/******/ 	"use strict";
/*!******************************************!*\
  !*** ./src/admin/editor/block-styles.ts ***!
  \******************************************/

// @ts-nocheck
// register alternate styles for core blocks
wp.domReady(() => {
    const buttonStyles = [
        {
            name: 'royal-blue-white-hover',
            label: 'Royal Blue with White Hover'
        },
        {
            name: 'white-royal-blue-hover',
            label: 'White with Royal Blue Hover'
        }
    ];
    const coverStyles = [
        {
            name: 'full-height-cover',
            label: '100% Height'
        },
        {
            name: 'red-shadow',
            label: 'Red Shadow'
        },
        {
            name: 'uconn-blue-shadow',
            label: 'Dark Blue Shadow'
        },
        {
            name: 'royal-blue-shadow',
            label: 'Royal Blue Shadow'
        },
        {
            name: 'grey-shadow',
            label: 'Grey Shadow'
        }
    ];
    const headingStyles = [
        {
            name: 'h2-small',
            label: 'H2 - Small'
        }
    ];
    // remove the core style choices from the button
    wp.blocks.unregisterBlockStyle('core/button', ['fill', 'outline']);
    buttonStyles.forEach(style => wp.blocks.registerBlockStyle('core/button', style));
    coverStyles.forEach(style => wp.blocks.registerBlockStyle('core/cover', style));
    headingStyles.forEach(style => wp.blocks.registerBlockStyle('core/heading', style));
});

/******/ })()
;
//# sourceMappingURL=block-style-overrides.js.map