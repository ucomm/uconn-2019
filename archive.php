<?php 
    get_header(); 

    $archive_type = $helpers->get_archive_type();
    
?>
    <main role="main" aria-label="Content" id="main-content">
        <section id="archive">
            <h1><?php esc_html_e( $archive_type['title'], 'uconn-2019' ); ?></h1>
            <?php get_template_part('template-parts/content', 'loop'); ?>
        </section>
    </main>

<?php

    include_once(UCONN_2019_DIR . '/template-parts/pagination.php');

    get_footer(); 
    
?>