<!-- search -->
<form role="search" class="search" method="get" action="<?php echo esc_url( home_url() ); ?>">
    <input class="search-input" type="search" name="s" aria-label="Search site for:" placeholder="<?php esc_html_e( 'To search, type and hit enter.', 'uconn-2019' ); ?>">
    <button class="search-submit" type="submit">
        <?php esc_html_e( 'Search', 'uconn-2019' ); ?>
    </button>
</form>
<!-- /search -->