<?php get_header(); ?>

    <main role="main" aria-label="Content" id="main-content">
        <section>
            <h1><?php esc_html_e( 'Category: ', 'uconn-2019' ); single_cat_title(); ?></h1>
            <?php get_template_part('template-parts/content', 'loop'); ?>
        </section>
    </main>

<?php get_footer(); ?>