<?php get_header(); ?>

    <main role="main" aria-label="Content" id="main-content">
        <section>
            <h1><?php esc_html_e( 'Index', 'uconn-2019' ); ?></h1>
            <?php get_template_part('template-parts/content', 'loop'); ?>
        </section>
    </main>

<?php get_footer(); ?>