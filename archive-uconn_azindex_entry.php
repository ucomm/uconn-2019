<?php
get_header();
if (!isset($helpers) || !class_exists('UConn2019\Lib\Helpers')) {
    include UCONN_2019_DIR . '/lib/Helpers.php';
    $helpers = new \UConn2019\Lib\Helpers();
}
$az_index_letters = array(
    'a' => array('display' => 'A', 'entries' => array()),
    'b' => array('display' => 'B', 'entries' => array()),
    'c' => array('display' => 'C', 'entries' => array()),
    'd' => array('display' => 'D', 'entries' => array()),
    'e' => array('display' => 'E', 'entries' => array()),
    'f' => array('display' => 'F', 'entries' => array()),
    'g' => array('display' => 'G', 'entries' => array()),
    'h' => array('display' => 'H', 'entries' => array()),
    'i' => array('display' => 'I', 'entries' => array()),
    'j' => array('display' => 'J', 'entries' => array()),
    'k' => array('display' => 'K', 'entries' => array()),
    'l' => array('display' => 'L', 'entries' => array()),
    'm' => array('display' => 'M', 'entries' => array()),
    'n' => array('display' => 'N', 'entries' => array()),
    'o' => array('display' => 'O', 'entries' => array()),
    'p' => array('display' => 'P', 'entries' => array()),
    'q' => array('display' => 'Q', 'entries' => array()),
    'r' => array('display' => 'R', 'entries' => array()),
    's' => array('display' => 'S', 'entries' => array()),
    't' => array('display' => 'T', 'entries' => array()),
    'u' => array('display' => 'U', 'entries' => array()),
    'v' => array('display' => 'V', 'entries' => array()),
    'w' => array('display' => 'W', 'entries' => array()),
    'x' => array('display' => 'X', 'entries' => array()),
    'y' => array('display' => 'Y', 'entries' => array()),
    'z' => array('display' => 'Z', 'entries' => array()),
);
?>

<main>
    <?php $helpers->get_angled_header('A-Z Index'); ?>
    <section>
        <?php
        if (have_posts() && function_exists('uconn_azindex_run_actions') && function_exists('get_field')) :
            while (have_posts()) :
                the_post();
                $starts_with_letter = strtolower(substr(get_the_title(), 0, 1));
                $custom_override = strtolower(get_field('letter_group'));

                if ($custom_override && array_key_exists($custom_override, $az_index_letters)) {
                    array_push($az_index_letters[$custom_override]['entries'], $post);
                } else if (array_key_exists($starts_with_letter, $az_index_letters)) {
                    array_push($az_index_letters[$starts_with_letter]['entries'], $post);
                }
            endwhile;
            /** FOR DEVELOPERS
             * MAKE SURE YOU RECOGNIZE THAT $post IS SET TO THE LAST ITEM AT THIS POINT
             * THIS WAS BECAUSE WE DID SOME PRE-PROCESSING THAT REQUIRED US TO LOOP
             * AND WE DON"T WANT TO REPEAT
             */
        ?>
            <div id="az-index-wrapper">
                <h2>Looking for something?</h2>
                <p>To search this list, type a keyword OR click a letter to jump to that letter’s listing.</p>
                <p>To request edits or additions to the A-Z index, please <a href="https://uconnucomm.wufoo.com/forms/uconn-webmanager-ticketing-form/">submit a request to the webmanager</a>.</p>

                <input id="az-search" name="az-search" type="search" value="" placeholder="Type here to filter this list" aria-label="use this search to filter links list" />
                <p id="results-text"><span class="count">0</span> result<span class="is-plural">s</span> found.</p>
                <ul id="link-list">
                    <?php foreach ($az_index_letters as $anchor => $letter_data) : ?>
                        <li>
                            <?php if (!empty($letter_data['entries'])) : ?>
                                <a href="#<?php echo $anchor; ?>"><?php echo $letter_data['display']; ?></a>
                            <?php else : ?>
                                <?php echo $letter_data['display']; ?>
                            <?php endif; ?>
                        </li>
                    <?php endforeach; ?>
                </ul>

                <?php foreach ($az_index_letters as $anch => $data) : ?>
                    <?php if (!empty($data['entries'])) : ?>
                        <div class="az-container">
                            <h3 id="<?php echo $anch; ?>"><?php echo $data['display']; ?></h3>
                            <ul class="az-link-list">
                                <?php foreach ($data['entries'] as $entry) : ?>
                                    <li><a href="<?php echo get_field('url', $entry->ID); ?>" target="_blank" rel="noopener noreferrer"><?php echo $entry->post_title; ?></a></li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>
            </div>
        <?php
        else :
            get_template_part('template-parts/content', 'none');
        endif;
        ?>

    </section>
</main>

<?php
get_footer(); ?>