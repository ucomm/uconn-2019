<?php get_header(); ?>

    <main role="main" aria-label="Content" id="main-content">
        <section>
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <div class="title-container">
                        <h1><?php the_title(); ?></h1>
                    </div>
                    <div class="image-container">
                        <?php the_post_thumbnail(); ?>
                    </div>
                    <div class="content-container">
                        <?php the_content(); ?>
                    </div>
                    <?php edit_post_link(); ?>
                </article>
            <?php endwhile; ?>

            <?php else : ?>
                <?php get_template_part('template-parts/content', 'none'); ?>
            <?php endif; ?>
        </section>
    </main>

<?php get_footer(); ?>