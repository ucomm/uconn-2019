<?php

/**
 * 
 * taken from https://codex.wordpress.org/Function_Reference/paginate_links. 
 * if someone has a better way of doing this, please go for it! - AB
 * 
 */

global $wp_query;
$huge = 999999999;

$translated = __('Page', 'uconn-theme');
$pageinate_args = array(
    'base' => str_replace($huge, '%#%', esc_url(get_pagenum_link($huge))),
    'format' => '?paged=%#%',
    'current' => max(1, get_query_var('paged')),
    'total' => $wp_query->max_num_pages
);

?>

<div id="pagination-container" class="contain pagination-container">
  <?php  
    echo paginate_links($pageinate_args);
  ?>
</div>