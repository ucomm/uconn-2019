<?php 
// This loop is intended for people taxonomy archive pages


if ( have_posts() ) : 
    while ( have_posts() ) : 
        the_post();
        $url = get_the_permalink();
    ?>
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <div class="post-content-wrapper person-info">
            <div class="image-container person-photo-container">
                <a href="<?php echo $url; ?>" aria-hidden="true">
                    <?php
                        echo $helpers->safe_featured_image($post, '/assets/img/placeholder-uconn.jpg');
                    ?>
                </a>
            </div>
            <div class="post-content-container">
                <div class="post-content">
                    <h2>
                        <a href="<?php echo $url; ?>" 
                            title="<?php the_title_attribute(); ?>">
                            <?php the_title(); ?>
                        </a>
                    </h2>
                </div>
            </div>
        </div>
    </article>
<?php 
    endwhile;
    else : 
        get_template_part('template-parts/content', 'none');
endif;