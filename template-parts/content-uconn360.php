<?php 
    use UConn2019\Lib\Helpers;

    if (!$helpers) {
        $helpers = new Helpers();
    }
    
    $url = get_field('podcast_url');
    $tags = get_the_terms(get_the_ID(), 'post_tag');
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="post-content-wrapper">
        <div class="image-container">
            <a href="<?php echo $url; ?>">
            <?php
                echo $helpers->safe_featured_image($post);
            ?>  
            </a>
        </div>
        <div class="post-content-container">
            <div class="post-content">
                <h2>
                    <a href="<?php the_permalink(); ?>" 
                        title="<?php the_title_attribute(); ?>">
                        <?php the_title(); ?>
                    </a>
                </h2>
                <?php
                    if ($tags !== false) {
                ?>
                        <div class="tag-container">
                            <p><strong>Tags:</strong></p>
                            <ul class="tag-list">
                            <?php
                                foreach ($tags as $tag) {
                            ?>
                                <li class="tag-list-item tag-item-<?php echo $tag->slug; ?>">
                                    <a href="/tag/<?php echo $tag->slug; ?>">
                                        <?php echo $tag->name; ?>
                                    </a>
                                </li>
                            <?php
                                }
                            ?>
                            </ul>
                        </div>
                <?php 
                    } 
                    
                    the_excerpt();
                ?>
                <p>
                    <strong>
                        <a aria-label="Listen to <?php echo the_title(); ?> or read a transcript" href="<?php echo $url; ?>">
                            Listen to the full podcast or read a transcript version
                        </a>
                    </strong>
                </p>
            </div>
        </div>
    </div>
</article>