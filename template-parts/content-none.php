<article id="content-none" <?php post_class(); ?>>
    <?php get_search_form(); ?>
    <p><a href="<?php echo esc_url( home_url() ); ?>"><?php esc_html_e( 'Return to the homepage.', 'uconn-2019' ); ?></a><p>
</article>