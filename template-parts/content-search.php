<?php // This loop is intended for locations where there are lists of posts.  Not singles or pages. ?>

<?php 
if ( have_posts() ) : 
    while ( have_posts() ) : 
        the_post(); 
    ?>
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <div class="post-content-wrapper">
            <div class="post-content-container">
                <div class="post-content">
                    <h3>
                        <a href="<?php the_permalink(); ?>" 
                            title="<?php the_title_attribute(); ?>">
                            <?php the_title(); ?>
                        </a>
                    </h3>
                    <?php 
                        the_excerpt(); 
                    ?>
                    <p>
                        <strong>
                            <a aria-label="Read more about <?php echo the_title(); ?>" href="<?php the_permalink(); ?>">
                                Read more
                            </a>
                        </strong>
                    </p>            

                </div>
            </div>
        </div>
    </article>
<?php 
    endwhile;
    else : 
        get_template_part('template-parts/content', 'none');
endif;