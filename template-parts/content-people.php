<?php

use UConn2019\Lib\UCPeople;

/**
 * If ACF isn't active, bail immediately.
 */
if (!is_plugin_active('advanced-custom-fields-pro/acf.php') && !is_plugin_active('advanced-custom-fields/acf.php')) {
  return;
}

if (!isset($ucPeople)) {
  $ucPeople = new UCPeople();
}

$fields = [];

if (is_array($args) && count($args) > 0) {
  $fields = $args;
} else {
  $fields = get_fields();
}

$tagIDs = isset($fields['specific_tags']) && is_array($fields['specific_tags']) ? $fields['specific_tags'] : [];
$groupIDs = isset($fields['specific_groups']) && is_array($fields['specific_groups']) ? $fields['specific_groups'] : [];
$peopleIDs = isset($fields['specific_people']) && is_array($fields['specific_people']) ? $fields['specific_people'] : [];
$infoToDisplay = isset($fields['information_to_display']) ? $fields['information_to_display'] : [];
$usesGroups = isset($fields['break_into_groups']) ? $fields['break_into_groups'] : false;

$displayConfig = [
  'layout' => $fields['layout'],
  'perRow' => $fields['persons_per_row'],
  'usesGroups' => $usesGroups,
  'infoToDisplay' => $infoToDisplay
];

$groupInfo = $ucPeople->getTaxonomyInfo('group', $groupIDs);
$tagInfo = $ucPeople->getTaxonomyInfo('persontag', $tagIDs);

if (count($peopleIDs) > 0) {
  $displayConfig['peopleIDs'] = $peopleIDs;
  $ucPeople->displayPeople($displayConfig);
}

if (count($groupIDs) > 0) {
  $fullIDList = [];
  foreach ($groupInfo as $info) {  
    if ($usesGroups) {
      $displayConfig['peopleIDs'] = $info->peopleIDs;
      $displayConfig['taxSlug'] = $info->taxSlug;
      $ucPeople->displayTaxInfo($info);
      $ucPeople->displayPeople($displayConfig);
    } else {
      array_push($fullIDList, $info->peopleIDs);
    }
  }

  if (!$usesGroups) {
    $idList = array_unique(array_merge(...$fullIDList));
    $displayConfig['peopleIDs'] = $idList;
    $ucPeople->displayPeople($displayConfig);
  }
}

if (count($tagIDs) > 0) {
  $fullIDList = [];
  foreach ($tagInfo as $info) {
    if ($usesGroups) {
      $displayConfig['peopleIDs'] = $info->peopleIDs;
      $displayConfig['taxSlug'] = $info->taxSlug;
      $ucPeople->displayTaxInfo($info);
      $ucPeople->displayPeople($displayConfig);
    } else {
      array_push($fullIDList, $info->peopleIDs);
    }
  } 
  if (!$usesGroups) {
    $idList = array_unique(array_merge(...$fullIDList));
    $displayConfig['peopleIDs'] = $idList;
    $ucPeople->displayPeople($displayConfig);
  }
}
