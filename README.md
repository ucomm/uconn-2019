# UConn 2019 Theme

This is is a new theme for uconn.edu, developed in 2019.

## Get Started

```bash
$ git clone git@bitbucket.org:ucomm/uconn-2019.git
$ cd uconn-2019
$ composer install
$ nvm use 
$ npm install
```

### To start development/see preview:

```bash
$ docker-compose up
$ open http://localhost
```

### To start JS/CSS
```bash
$ nvm use # only if you're in a new terminal context
$ npm run dev
```

JS/CSS assets are managed with webpack. Locally, they are built into the `build` directory. On dev and prod servers, they will be in the `dist` directory.

## Adding Plugins/Dependencies

This project uses composer as a dependency management tool for PHP.  It also uses NPM/Yarn as a dependency management tool for local build tools, to compile/minify CSS/JS.

Packages can be found:

- [Composer Packagist](https://packagist.org/)
- [WordPress Packagist](https://wpackagist.org/)
- [UConn Communications Packagist](https://packages.ucdev.net/)
- [NPM Packages (JavaScript)](https://www.npmjs.com/)

## Branching Strategy

We try to use [Git Flow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow) as our branching strategy.  Not required, but helpful when working on a team.

Tags must follow the [semver system](http://semver.org/).

## Debugging

You can access Docker containers directly from the shell (as long as they are running), example:

```bash
docker-compose exec web bash
```