// @ts-nocheck
import './admin-variables.scss'
// initialize
let pageTemplate = null

const listenForPageTemplate = () => {
  
  // subscribe to changes in the editor's state
  wp.data.subscribe(() => {
    
    // when the state changes, get the page template
    const newPageTemplate = wp.data.select('core/editor').getEditedPostAttribute('template')

    // make sure that the correct value is read when the editor first loads
    // then continue updating whenever the template changes
    if (
      (newPageTemplate !== undefined && pageTemplate === null) || 
      (newPageTemplate !== undefined && newPageTemplate !== pageTemplate) 
      ) {
        
      pageTemplate = newPageTemplate

      pageTemplate.includes('angled-header') ?
        document.body.classList.add('angled-header-template') :
        document.body.classList.remove('angled-header-template')
    }

  })
}

listenForPageTemplate()

