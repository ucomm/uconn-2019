// @ts-ignore
import Navigation from 'navigation';
import bindMobileToggle from './mobilenav';
import azSearch from './az-search';

import '../sass/main.scss'

document.addEventListener('DOMContentLoaded', () => {
    // Add support for the A11y Menu
    const navigation = new Navigation({
        'menuId': 'am-main-menu',
        'click': true
    });
    navigation.init();

    bindMobileToggle();
    azSearch();

})