const toggleClass = (
  el: HTMLDivElement | HTMLLIElement, 
  isHidden: boolean
) => isHidden ? 
  el.classList.add('hidden') : 
  el.classList.remove('hidden')

const resultsTextHandler = (el: HTMLParagraphElement | null, count: number) => {
  if (!el) {
    return
  }
  const countSpan = el.querySelector('.count') as HTMLSpanElement
  const pluralSpan = el.querySelector('.is-plural') as HTMLSpanElement
  if (count >= 0) {
    el.style.visibility = 'visible'
    countSpan.innerText = `${count}`
    pluralSpan.innerText = count === 1 ? '' : 's'
  } else {
    el.style.visibility = 'hidden'
    countSpan.innerText = '0'
    pluralSpan.innerText = 's'
  }
}

const azSearch = () => {
  const input = document.querySelector('#az-search') as HTMLInputElement;
  
  if (!input) {
    return
  }
  
  const containers = Array.from(document.querySelectorAll('.az-container')) as HTMLDivElement[]
  const items = Array.from(document.querySelectorAll('.az-link-list li')) as HTMLLIElement[]
  const resultsText = document.querySelector('#results-text') as HTMLParagraphElement
  const count = items.length

  input.addEventListener('keyup', (evt: KeyboardEvent) => {

    const target = evt.target as HTMLInputElement
    if (evt.code === 'Escape') {
      /**
       * 
       * if the esc key is pressed while the mobile menu is available,
       * the key press should not trigger the menu to open.
       * stopImmediatePropagation prevents the event from bubbling beyond the input
       * 
       */
      evt.stopImmediatePropagation()
      target.value = ''
    }

    if (target.value.length > 1) {
      const searchTerm = target.value.trim().toLowerCase()

      // hide items that don't match the search term
      const filteredItems = items.filter(item => {
        toggleClass(item, false)

        if (!item.innerText.toLowerCase().includes(searchTerm)) {
          toggleClass(item, true)
          return item
        } 
      })

      // manage list containers
      containers.map(container => {
        const items = container.querySelectorAll('li')
        const hiddenItems = container.querySelectorAll('li.hidden') as NodeListOf<HTMLLIElement>
        const shownItems = container.querySelectorAll('li:not(.hidden)') as NodeListOf<HTMLLIElement>
        
        // if there's only one item, force it to the left of the page
        if (shownItems.length === 1) {
          shownItems[0].style.columnSpan = 'all'
        }
        
        // check to see if there are any items still visible in the container
        // if not, hide the container
        items.length === hiddenItems.length ?
          toggleClass(container, true) :
          toggleClass(container, false)
      })

      // show how many results there are
      resultsTextHandler(resultsText, count - filteredItems.length)

    } else {
      // hide the results text and show all items

      resultsTextHandler(resultsText, -1)
      items.map(i => toggleClass(i, false))
      containers.map(c => toggleClass(c, false))

    }
  })
}

export default azSearch