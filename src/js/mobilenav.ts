const toggleMobileMenu = (evt: KeyboardEvent | MouseEvent) => {
    const type = evt.type
    const activeNav = document.querySelector('.mobile-navigation-container') as HTMLDivElement
    switch (type) {
        case 'click':
            activeNav.classList.toggle('active')
            break;
        case 'keyup':
            const { code } = evt as KeyboardEvent
            if (code === 'Escape' && activeNav) {
                activeNav.classList.toggle('active')
            } else {
                return
            }
            break;
        default:
            return;
    }
}



const bindMobileToggle = () => {
    const mobileTrigger = document.querySelector('.mobile-trigger') as HTMLButtonElement
    const mobileClose = document.querySelector('.mobile-navigation-close') as HTMLButtonElement
    mobileTrigger.addEventListener('click', toggleMobileMenu)
    mobileClose.addEventListener('click', toggleMobileMenu)
    document.addEventListener('keyup', toggleMobileMenu)
}

export default bindMobileToggle;