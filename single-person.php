<?php

if (!isset($helpers) || !class_exists('UConn2019\Lib\Helpers')) {
  include UCONN_2019_DIR . '/lib/Helpers.php';
  $helpers = new \UConn2019\Lib\Helpers();
}

get_header();

$fields = function_exists('get_fields') ? get_fields() : false;

$groupInfo = $ucPeople->getPersonTermInfo(get_the_ID(), 'group');
$personTagInfo = $ucPeople->getPersonTermInfo(get_the_ID(), 'persontag');

$sidebarFields = [
  [
    'value' => $fields['email'],
    'label' => 'Email',
    'hasLink' => true,
    'link' => 'mailto:' . $fields['email']
  ],
  [
    'value' => $fields['phone'],
    'label' => 'Phone',
    'hasLink' => false,
    'link' => null
  ],
  [
    'value' => $fields['office_location'],
    'label' => 'Office Location',
    'hasLink' => false,
    'link' => null
  ],
  [
    'value' => $fields['campus'],
    'label' => 'Campus',
    'hasLink' => false,
    'link' => null
  ],
  [
    'value' => $fields['url'],
    'label' => 'Link',
    'hasLink' => true,
    'link' => $fields['url']
  ],
  [
    'value' => $groupInfo,
    'label' => 'Groups',
    'hasLink' => null,
    'link' => null
  ],
  [
    'value' => $personTagInfo,
    'label' => 'Tags',
    'hasLink' => null,
    'link' => null
  ]
];

?>

<main aria-label="Content" id="main-content">
  <section>
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
          <?php
            echo $helpers->get_angled_header(get_the_title());
          ?>
          <section class="info-section">
            <div class="headshot-container">
              <div class="image-container">
                <?php the_post_thumbnail(); ?>
              </div>
              <div aria-hidden="true" class="title-container">
                <h2><?php the_title(); ?></h2>
                <p class="person-title">
                  <?php echo $ucPeople->breakField($fields['title']); ?>
                </p>
                <p class="person-department">
                  <?php echo $ucPeople->breakField($fields['department']); ?>
                </p>
              </div>
            </div>
            <div class="info-container">
              <?php
              foreach ($sidebarFields as $sidebarItem) {
                get_template_part('partials/people/single', 'person-sidebar-info', $sidebarItem);
              }
              ?>
            </div>
          </section>
          <section class="bio-section">
            <div class="content-container">
              <?php echo $fields['about']; ?>
            </div>
            <?php edit_post_link(); ?>
          </section>
        </article>
      <?php endwhile; ?>

    <?php else : ?>
      <?php get_template_part('template-parts/content', 'none'); ?>
    <?php endif; ?>
  </section>
</main>


<?php
get_footer();
