<?php

get_header();
$archive_type = $helpers->get_archive_type();
?>

<main id="main-content" aria-label="Content">
  <section id="archive">
    <h1>
      <?php esc_html_e($archive_type['title'], 'uconn-2019'); ?>
    </h1>
    <?php
    UConn2019\Lib\Menus::display_podcast_navigation();
    ?>

    <?php
    $terms = get_terms([
      'taxonomy' => 'post_tag',
      'orderby' => 'name'
    ]);

    if ($terms) {
    ?>
      <form action="/uconn360-podcast" method="GET" id="tax-filter-form">
        <div>
          <div>
            <label for="tag-filter">Filter podcasts by tag</label>
          </div>
          <select name="tag-filter" id="tag-filter" class="wpcf7-form-control wpcf7-select">
            <option value="" autocomplete="off">Display All</option>
            <?php
            foreach ($terms as $term) {
              $selected = isset($_GET['tag-filter']) && $_GET['tag-filter'] === $term->slug ? 'selected="selected"' : '';
            ?>
              <option <?php echo $selected; ?> value="<?php echo $term->slug; ?>" autocomplete="off">
                <?php echo $term->name; ?>
              </option>
            <?php
            }
            ?>
          </select>
        </div>
        <div>
          <input type="submit" value="Search">
        </div>
      </form>
    <?php
    }
    ?>
    <div id="result">
      <?php
      if (have_posts()) :
        while (have_posts()) :
          the_post();
          get_template_part('template-parts/content', 'uconn360');
        endwhile;
      else :
        get_template_part('template-parts/content', 'none');
      endif;
      ?>
    </div>
  </section>
</main>

<?php

include_once(UCONN_2019_DIR . '/template-parts/pagination.php');

get_footer();

?>