<?php get_header(); ?>

    <main role="main" aria-label="Content" id="main-content">
        <section>
            <main id="main-content" aria-label="Content">
                <h1><?php esc_html_e( 'Tag: ', 'uconn-2019' ); single_tag_title( '', true ); ?></h1>
                <section id="archive">
                    <?php get_template_part('template-parts/content', 'loop'); ?>
                </section>
            </main>
        </section>
    </main>

<?php get_footer(); ?>