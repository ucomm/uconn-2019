pipeline {
  agent any
  environment {
    THEME_SLUG = "uconn-2019"
    BITBUCKET_SLUG = "uconn-2019"
    DEV_BRANCH = "develop"
    FEATURE_BRANCH = "feature/*"
    PROD_BRANCH = "master"
  }
  // handle rsync dry runs
  parameters {
    booleanParam(
      name: 'RSYNC_DRY_RUN',
      defaultValue: true,
      description: 'Toggles the rsync --dry-run flag'
    )
  }
  stages {
    stage('Checkout') {
      steps {
        // do not delete builds. there's a breaking issue there
        // https://issues.jenkins.io/browse/JENKINS-66843
        scmSkip(deleteBuild: false)
        sendNotifications 'STARTED'
      }
    }
    stage("Prepare Build Assets") {
      when {
        anyOf {
          branch "${DEV_BRANCH}";
          branch "${FEATURE_BRANCH}";
          branch "${PROD_BRANCH}";
          buildingTag()
        }
      }
      parallel {
        stage('NPM') {
          steps {
            sh "${WORKSPACE}/ci-scripts/npm.sh"
          }
        }
        stage('Composer') {
          steps {
            sh "${WORKSPACE}/ci-scripts/composer.sh"
          }
        }
      }
      post {
        always {
          echo "======== $BRANCH_NAME end asset builds ========"
        }
        success {
          echo "======== success - $BRANCH_NAME asset builds ========"
        }
        failure {
          echo "======== failed - $BRANCH_NAME asset builds ========"
        }
      }
    }
    stage('Dev Pushes') {
      environment {
        FILENAME = "$GIT_BRANCH"
        SITE_DIRECTORY = "edu.uconn.devredesign"
      }
      when {
        anyOf {
          branch "${DEV_BRANCH}";
          branch "${FEATURE_BRANCH}"
        }
      }
      parallel {
        stage('Push dev to staging0') {
          steps {
            sh "${WORKSPACE}/ci-scripts/dev-push.sh"
          }
        }
        stage('Archive dev to bitbucket') {
          steps {
            script {
              // only archive dev branches
              if (env.BRANCH_NAME == "${DEV_BRANCH}") {
                sh "${WORKSPACE}/ci-scripts/bitbucket-archive.sh"
              }
            }
          }
        }
      }
    }
    stage('Staging Push') {
      environment {
        SITE_DIRECTORY = "edu.uconn.staging"
      }
      when {
        branch "${PROD_BRANCH}"
      }
      steps {
        sh "${WORKSPACE}/ci-scripts/dev-push.sh"
      }
      post {
        failure {
          sendNotifications("FAILURE - Push to staging")
        }
      }
    }
    stage('Prod Push') {
      environment {
        SITE_DIRECTORY = "edu.uconn"
        FILE_NAME = "${TAG_NAME}"
      }
      when {
        buildingTag()
      }
      parallel {
        stage ('Push tag to prod') {
          steps {
            sh "${WORKSPACE}/ci-scripts/prod-push.sh"
          }
        }
        stage('Archive tag to bitbucket') {
          steps {
            script {
              if (env.RSYNC_DRY_RUN == "false") {
                sh "${WORKSPACE}/ci-scripts/bitbucket-archive.sh"
              }
            }
          }
        }
      }
      post {
        failure {
          sendNotifications("FAILURE - Push to prod")
        }
        success {
          // NB - don't use script blocks very much.
          // I'm just adding this so th comm0-updates channel doesn't get flooded on dry runs
          script {
            if (env.RSYNC_DRY_RUN == "false") {
              sendNotifications("SUCCESS", "#comm0-updates", [
                [
                  type: "section",
                  text: [
                    type: "mrkdwn",
                    text: ":tada: *$THEME_SLUG* updated"
                  ]
                ]
              ])
              slackUploadFile(channel: "#comm0-updates", filePath: "changelog.md", initialComment:  "uconn-2019 Changelog")
            }
          }
        }
      }
    }
  }
  post {
    success {
      sendNotifications 'SUCCESSFUL'
    }
    failure {
      sendNotifications 'FAILED'
    }
    aborted {
      sendNotifications 'ABORTED'
    }
    always {
      echo "======== Cleanup ========"
      sh "rm -rf node_modules"
    }
  }
}