<?php
/*
Template Name: Search Page
*/
?>

<?php
global $wp;

$q = isset($_GET['q']) ? $_GET['q'] : '';
$cx = isset($_GET['cx']) ? sanitize_text_field($_GET['cx']) : '004595925297557218349:65_t0nsuec8';


// Required for phonebook searching.
if($cx === 'people' && $q !== ''){
    header('Location: https://phonebook.uconn.edu/lresults.php?' . urlencode($q));
    die();
}

$radios = array(
    (object) array(
        'name' => 'UConn',
        'id'    => 'Radio_Search_UConn',
        'value' => '004595925297557218349:65_t0nsuec8',
    ),
    (object) array(
        'name' => 'The Web',
        'id'    => 'Radio_Search_Web',
        'value' => '004595925297557218349:88uyjxwpn_0',
    ),
    (object) array(
        'name' => 'People',
        'id'    => 'Radio_Search_People',
        'value' => 'people',
    )
);

if (!isset($helpers) || !class_exists('UConn2019\Lib\Helpers')) {
    include UCONN_2019_DIR . '/lib/Helpers.php';
    $helpers = new \UConn2019\Lib\Helpers();
}
?>

<?php 
    add_filter('body_class', array('UConn2019\Lib\Helpers', 'add_angled_header_class'));
    get_header(); 
?>

<main role="main" id="main-content">
    <?php echo $helpers->get_angled_header('Search'); ?>
    <section class="search-contain">
    <?php the_post(); ?>
        <?php the_content(); ?>
        <form id="google-search" method="GET" action="<?php echo add_query_arg( $wp->query_string, '', home_url( $wp->request ) ); ?>">

            <div>
            <?php foreach($radios as $key => $radio): ?>
                <label>
                    <input name="cx" id="<?php echo $radio->id ?>" type="radio" value="<?php echo $radio->value ?>" <?php if($cx === $radio->value): ?>checked="checked"<?php endif; ?>/>
                    <?php echo $radio->name ?>
                </label>
            <?php endforeach; ?>
            </div>
            <div>
                <div>
                    <input aria-label="search the University of Connecticut" type="search" placeholder="Search UConn..." id="q" name="q" value="<?php echo htmlspecialchars($q)?>"></div>
                <div>
                    <button id="sa" type="submit" name="sa" value="">Search</button>
                </div>
            </div>
            <input name="cof" type="hidden" value="FORID:10" />
            <input name="ie" type="hidden" value="UTF-8" />
        </form>

        <script>
            (function() {
                var cx = '<?php echo $cx ?>';
                var gcse = document.createElement('script');
                gcse.type = 'text/javascript';
                gcse.async = true;
                gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
                    '//www.google.com/cse/cse.js?cx=' + cx;
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(gcse, s);
            })();
        </script>
        
        <div id="cse-search-results">
            <gcse:searchresults-only></gcse:searchresults-only>
        </div>
    </section>
</main>

<?php get_footer(); ?>

