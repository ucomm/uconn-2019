# Changelog
## 1.7.8
- Fix for undefined array key in podcast archive page
## 1.7.7
- Fix to prevent helpers class from calling static method improperly.
## 1.7.6
- Fix to search page to make sure that people can't add their own messages to it...
- Temporary addition of Forminator plugin styles. These will remain until we can figure out why the styles aren't being correctly sourced on the new server.
## 1.7.5
- Removed Forminator plugin filter to grant form access to editors. This functionality is now part of their plugin on a per site basis
- Added `#f0f3f7` as a color option which had accidentally been removed in a previous update
## 1.7.4
- Updated button styles to match new "blue" theme
  - Added support for new white background/royal blue hover button style
## 1.7.3
- Updated footer styles
  - Social icons now match the main footer color
  - On hover, social icon backgrounds are royal blue and the interiors are white
## 1.7.2
- Updated podcast archive items to include information about transcripts
## 1.7.1
- Updated header typography
## 1.7.0
- Updated with new Heavy/blue styles
- Updated `theme.json` with more theme details
- Added local fonts
  - PT Serif
  - PT Serif Caption
  - Raleway
  - Roboto
  - uconn
- Deprecated tailwindcss
## 1.6.5
- Updated object storage URLs from stackpath (rip) to azure
## 1.6.4
- Hotfix to remove unexpected PHP output in non-HTTP contexts
## 1.6.3
- Updated page title rendering (it's handled via plugin)
## 1.6.2
- Removed meta description from header (it's handled via plugin)
## 1.6.1
- Removed fontawesome composer dependency
- Removed unneeded style asset functions
  - dequeuing shiftnav fontawesome css (plugin not used any more)
  - conditionally loading fontawesome based on beaver builder
## 1.6.0
- Updated customizer to accept CSS class names instead of selectors
- Improved support for FontAwesome icons
## 1.5.7
- Fixed issue with composer not loading classes correctly
## 1.5.6
- Added styles for slate forms. Forms supplied by the `uc-slate` plugin
## 1.5.5
- Support for `uc-people-compat` plugin
## 1.5.4
- Added `uconn-2019` class to `<body>`
- Fixed typography to prevent styles being changed by custom icons
## 1.5.3
- Fixed issue where `ucomm-people` shortcodes could crash pages if bad/wrong tax slugs were added.
## 1.5.2
- Changed labels for people info
- Updated styles of angled header on people pages
- Updated custom taxonomy archive pages to match style and output of `user-people.php`
- Improved query of people on taxonomy pages
## 1.5.1
- Fix for nav menu items not appearing on person taxonomy pages
## 1.5.0
- Added angled header to people pages
- Added customizer support for changing footer social icons
- Added explicit `text-decoration` style to links because BB is being unreasonable
- Fixed mobile nav underlines
- Updated space for people info items
- Fixed people template not displaying people due to plugin name change
- Added support for jenkins multibranch pipelines
## 1.4.6
- Fixed error checking on `UCPeople->getPersonTermInfo` to avoid fatal errors
## 1.4.5
- Added support for editors to embed iframes via unfiltered HTML
- Added `uc-forminator-admin` class to allow hiding forminator form fields to non-logged in users
## 1.4.4
- Added support for editors to use Forminator Pro
## 1.4.3
- Fix for ACF fields not handling `&`'s well
- Added extra heading style for gutenberg
- Fixed fatal error for people template pages
- Fixed styling for people template pages
- Added filters for tags, groups, and names on people template pages
## 1.4.2
- Added support for persontag/group archive pages
- Improved people page styles
- Added support for small h2 version using class name
## 1.4.1
- Fixed style bug for menu buttons
- Updated support for cache detection and management
## 1.4.0
- Removed support for WP Cassify
- Updated font family CDN url
- Updated cookie notice script url
- Added support for serving Beaver Builder background images over CDN
## 1.3.6
- Fixed style for beaver builder buttons
## 1.3.5
- Fix to use minified a11y menu navigation JS file
## 1.3.4
- Typography fix for header line heights
## 1.3.3
- Style fixes
  - angled header title and width
  - search page widths
## 1.3.2
- Style fix for uconn360 podcast archive
## 1.3.1
- Support for alternate BB button styles
## 1.3.0
- Added fontawesome composer package support
- General style updates
- Added gutenberg support
  - Editor styles
  - Body class when blocks are detected
  - Layout styles
  - Fix for BB using a block to create its layout
  - Registered block patterns
  - Angle template header appears in editor
  - Custom style options for some core blocks in editor
- Support for uc-people plugin
  - Failsafe if uc-people and ACF aren't active
  - Checks for uc-people plugin to avoid site crashes
  - Template for people page
  - Added shortcode uc-people to theme for increased flexibility
- Support for wp-cassify plugin
## 1.2.1
- Style updates to banner icons to resolve some bugs 
## 1.2.0
- Updated script/style loading to support child themes 
- Added support for tailwindCSS
- Added support for local Beaver Builder Data Fix plugin
- Added support for tiktok icon
## 1.1.11
- Updates to composer.json including WP core
- Added info to submit requests to webmanager
## 1.1.10
- Added styles for gravity forms plugin
- Plugin and core updates
## 1.1.9
- Addition of AZ Index Customizations (archive, styles)
## 1.1.8
- Local development process changes
## 1.1.7
- Adjustments to safe featured image and small style changes for it
## 1.1.6
- Updated dependencies and small style tweaks for it.
## 1.1.5
- Added support for a tag filter on post archive pages
## 1.1.4
- Added initial micro data for improved google search results.
## 1.1.3
- Added Jess' styles for Perception/Truth (fact check)
## 1.1.2
- Updated a few more things on composer
- Fixed logical issue for new local instance w/ menus
## 1.1.1
- Updates to composer.json
- Removed composer build process for js/sass assets
- Updated nav style to better support IE11
## 1.1.0
- Update to support alert banner
## 1.0.10
- Added Custom Meta Boxes back to WP admin
## 1.0.9
- Fixed main nav style
## 1.0.8
- Added A-Z/Search links to the mobile nav menu
## 1.0.7
- Re-enabled REST API on the theme level to be better handled by a plugin.
## 1.0.6
- Add a skip to content link and full anchor tag block area for the top left logo
## 1.0.5
- Fixed issue with a-z search that was causing problems with IE10/11
## 1.0.4
- Fixed console error on az input field
## 1.0.3
- Removed snapchat social icon from footer by request
- Fixed responsive style for uctoday plugin 
## 1.0.2
- Added screenshot.png
## 1.0.1
- Fix for angled banner titles
- Pre-production release
## 1.0.0
- First release