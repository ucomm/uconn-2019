<?php

add_filter('body_class', array('UConn2019\Lib\Helpers', 'add_angled_header_class'));
get_header();
if (!isset($helpers) || !class_exists('UConn2019\Lib\Helpers')) {
  include UCONN_2019_DIR . '/lib/Helpers.php';
  $helpers = new \UConn2019\Lib\Helpers();
}
?>
<main role="main" aria-label="Content" id="main-content">
  <?php echo $helpers->get_angled_header(get_the_archive_title()); ?>
  <section id="archive" class="people-container">
    <div class="people-grid tax-content-container">
      <?php
        $displayConfig = [
          'infoToDisplay' => [
            'photo',
            'first_name',
            'last_name',
            'title',
            'email',
            'phone'
          ],
          'layout' => 'grid',
          'perRow' => '4'
        ];

        $ids = $ucPeople->getPeopleIDsByTaxonomy('persontag');

        if (count($ids)) {
          $displayConfig['peopleIDs'] = $ids;
          $ucPeople->displayPeople($displayConfig);
        }

      ?>
    </div>
  </section>
</main>

<?php

include_once(UCONN_2019_DIR . '/template-parts/pagination.php');

get_footer();

?>