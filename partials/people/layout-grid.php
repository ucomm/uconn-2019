<?php

use UConn2019\Lib\UCPeople;

$ucPeople = new UCPeople();

$columns = 'people-cols-' . $args['perRow'];

$taxClass = isset($args['taxSlug']) && $args['taxSlug'] !== '' ? $args['taxSlug'] : '';

$gridClasses = $columns . ' ' . $taxClass;

$args['data'];

?>



<div id="people-grid" class="people-grid <?php echo $gridClasses; ?>" aria-live="polite" role="region" aria-label="Faculty/Staff List">
<?php
foreach ($args['data'] as $person) {
  $groupData = '';
  $personTagData = '';
  $thumbnail = get_the_post_thumbnail($person['id']);
  $permalink = get_the_permalink($person['id']);
  $groups = get_the_terms($person['id'], 'group');
  $personTags = get_the_terms($person['id'], 'persontag');

    if (false !== $groups && !is_wp_error($groups)) {
      foreach ($groups as $g) {
        $groupData .= $g->slug . ' ';
      }
    }

    if (false !== $personTags && !is_wp_error($personTags)) {
      foreach ($personTags as $t) {
        $personTagData .= $t->slug . ' ';
      }
    }
  ?>
    <div class="person-info" data-groups="<?php echo $groupData; ?>" data-persontags="<?php echo $personTagData; ?>">
      <?php
      if ($person['use_photo']) {
      ?>
        <div class="person-photo-container">
          <a href="<?php echo $permalink; ?>" aria-hidden="true">
            <?php
            echo $thumbnail !== '' ?
              $thumbnail :
              "<img src='" . UCONN_2019_URL . '/assets/img/placeholder-uconn.jpg' . "' class='attachment-post-thumbnail size-post-thumbnail wp-post-image' loading='lazy' />";
            ?>
          </a>
        </div>
      <?php
      }
      ?>
      <p class="person-detail person-name"><strong><a href="<?php echo $permalink; ?>"><?php echo $ucPeople->getFullName($person); ?></a></strong></p>
      <?php
      if ($person['title']) {
      ?>
        <p class="person-detail person-title"><?php echo $ucPeople->breakField($person['title']); ?></p>
      <?php
      }
      ?>
      <?php
      if ($person['email']) {
      ?>
        <p class="person-detail person-email">
          <a href="mailto:<?php echo $person['email']; ?>">
            <?php echo $person['email']; ?>
          </a>
        </p>
      <?php
      }
      ?>
      <?php
      if ($person['phone']) {
      ?>
        <p><?php echo $person['phone']; ?></p>
      <?php
      }
      ?>
      <?php
      if ($person['phone_alternate']) {
      ?>
        <p><?php echo $person['phone_alternate']; ?></p>
      <?php
      }
      ?>
    </div>
  <?php
  }
  ?>
</div>