<?php
if (!$args['value']) {
  return;
}
?>

<div class="person-info-item">
  <div class="item-label-container">
    <p class="item-label">
      <?php echo $args['label']; ?>
    </p>
  </div>
  <div class="item-container">
    <p class="item-value">
      <?php
      if (!$args['hasLink']) {
        echo $args['value'];
      } else {
      ?>
        <a class="item-link" href="<?php echo $args['link']; ?>"><?php echo $args['value']; ?></a>
      <?php
      }
      ?>
    </p>
  </div>
</div>