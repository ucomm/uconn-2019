<!-- wp:group {"align":"full"} -->
<div class="wp-block-group alignfull">
  <!-- wp:cover {"overlayColor":"ucBlue-100"} -->
  <div class="wp-block-cover"><span aria-hidden="true" class="has-uc-blue-100-background-color has-background-dim-100 wp-block-cover__gradient-background has-background-dim"></span>
    <div class="wp-block-cover__inner-container">
      <!-- wp:paragraph {"align":"center","placeholder":"Write title…","textColor":"ucBlue-900","fontSize":"large"} -->
      <p class="has-text-align-center has-uc-blue-900-color has-text-color has-large-font-size">You can replace this background color with an image.</p>
      <!-- /wp:paragraph -->
    </div>
  </div>
  <!-- /wp:cover -->
</div>
<!-- /wp:group -->