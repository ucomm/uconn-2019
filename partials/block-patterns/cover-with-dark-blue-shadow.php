<!-- wp:cover {"overlayColor":"ucBlue-100","minHeight":324,"minHeightUnit":"px","className":"is-style-uconn-blue-shadow"} -->
<div class="wp-block-cover is-style-uconn-blue-shadow" style="min-height:324px"><span aria-hidden="true" class="has-uc-blue-100-background-color has-background-dim-100 wp-block-cover__gradient-background has-background-dim"></span>
  <div class="wp-block-cover__inner-container">
    <!-- wp:paragraph {"align":"center","placeholder":"Write title…","textColor":"ucBlue-900","fontSize":"large"} -->
    <p class="has-text-align-center has-uc-blue-900-color has-text-color has-large-font-size">Replace the blue with an image and delete text as needed. Has a blue shadow.</p>
    <!-- /wp:paragraph -->
  </div>
</div>
<!-- /wp:cover -->