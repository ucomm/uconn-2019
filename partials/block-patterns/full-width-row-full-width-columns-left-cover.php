<!-- wp:group {"align":"full"} -->
<div class="wp-block-group alignfull">
  <!-- wp:columns {"className":"full-width-columns"} -->
  <div class="wp-block-columns full-width-columns">
    <!-- wp:column -->
    <div class="wp-block-column">
      <!-- wp:cover {"overlayColor":"ucBlue-100","minHeight":468,"minHeightUnit":"px","className":"is-style-full-height-cover"} -->
      <div class="wp-block-cover is-style-full-height-cover" style="min-height:468px"><span aria-hidden="true" class="has-uc-blue-100-background-color has-background-dim-100 wp-block-cover__gradient-background has-background-dim"></span>
        <div class="wp-block-cover__inner-container">
          <!-- wp:paragraph {"align":"center","placeholder":"Write title…","textColor":"ucBlue-900","fontSize":"large"} -->
          <p class="has-text-align-center has-uc-blue-900-color has-text-color has-large-font-size">Replace this cover with an image</p>
          <!-- /wp:paragraph -->
        </div>
      </div>
      <!-- /wp:cover -->
    </div>
    <!-- /wp:column -->

    <!-- wp:column {"style":{"spacing":{"padding":{"bottom":"20px"}}},"backgroundColor":"ucGrey-100","textColor":"ucGrey-800"} -->
    <div class="wp-block-column has-uc-grey-800-color has-uc-grey-100-background-color has-text-color has-background" style="padding-bottom:20px">
      <!-- wp:heading {"textColor":"ucGrey-800"} -->
      <h2 class="has-uc-grey-800-color has-text-color">Placeholder Heading</h2>
      <!-- /wp:heading -->

      <!-- wp:paragraph {"style":{"typography":{"fontSize":"28px"}}} -->
      <p style="font-size:28px">Placeholder secondary message</p>
      <!-- /wp:paragraph -->

      <!-- wp:paragraph -->
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer quis lacus a dui dapibus lobortis. Nulla nec condimentum ante. Donec vel massa et elit placerat scelerisque. Suspendisse gravida lacus viverra leo placerat sagittis. Aenean ultricies turpis tellus, et convallis tellus pretium quis. Morbi ex est, auctor sed dictum ac, laoreet eu eros. Curabitur finibus sodales nulla id varius. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae;</p>
      <!-- /wp:paragraph -->

      <!-- wp:buttons {"layout":{"type":"flex","justifyContent":"left"}} -->
      <div class="wp-block-buttons">
        <!-- wp:button -->
        <div class="wp-block-button"><a class="wp-block-button__link">CTA Button</a></div>
        <!-- /wp:button -->
      </div>
      <!-- /wp:buttons -->
    </div>
    <!-- /wp:column -->
  </div>
  <!-- /wp:columns -->
</div>
<!-- /wp:group -->