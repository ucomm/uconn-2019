<!-- wp:group {"align":"full"} -->
<div class="wp-block-group alignfull">
  <!-- wp:cover {"overlayColor":"ucBlue-100"} -->
  <div class="wp-block-cover"><span aria-hidden="true" class="wp-block-cover__background has-uc-blue-100-background-color has-background-dim-100 has-background-dim"></span>
    <div class="wp-block-cover__inner-container">
      <!-- wp:group {"layout":{"inherit":true}} -->
      <div class="wp-block-group">
        <!-- wp:columns -->
        <div class="wp-block-columns">
          <!-- wp:column -->
          <div class="wp-block-column"></div>
          <!-- /wp:column -->

          <!-- wp:column -->
          <div class="wp-block-column">
            <!-- wp:cover {"dimRatio":40,"overlayColor":"ucGrey-100","style":{"spacing":{"padding":{"top":"20px","right":"20px","bottom":"20px","left":"20px"}}}} -->
            <div class="wp-block-cover" style="padding-top:20px;padding-right:20px;padding-bottom:20px;padding-left:20px"><span aria-hidden="true" class="wp-block-cover__background has-uc-grey-100-background-color has-background-dim-40 has-background-dim"></span>
              <div class="wp-block-cover__inner-container">
                <!-- wp:paragraph {"align":"center","placeholder":"Write title…","textColor":"black","fontSize":"large"} -->
                <p class="has-text-align-center has-black-color has-text-color has-large-font-size">This is some text</p>
                <!-- /wp:paragraph -->
              </div>
            </div>
            <!-- /wp:cover -->
          </div>
          <!-- /wp:column -->
        </div>
        <!-- /wp:columns -->
      </div>
      <!-- /wp:group -->
    </div>
  </div>
  <!-- /wp:cover -->
</div>
<!-- /wp:group -->