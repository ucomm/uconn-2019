<!-- wp:group -->
<div class="wp-block-group">
  <!-- wp:columns -->
  <div class="wp-block-columns">
    <!-- wp:column {"width":"60%"} -->
    <div class="wp-block-column" style="flex-basis:60%">
      <!-- wp:heading -->
      <h2>Heading</h2>
      <!-- /wp:heading -->

      <!-- wp:paragraph -->
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer quis lacus a dui dapibus lobortis. Nulla nec condimentum ante. Donec vel massa et elit placerat scelerisque. Suspendisse gravida lacus viverra leo placerat sagittis. Aenean ultricies turpis tellus, et convallis tellus pretium quis. Morbi ex est, auctor sed dictum ac, laoreet eu eros. Curabitur finibus sodales nulla id varius. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae;</p>
      <!-- /wp:paragraph -->

      <!-- wp:paragraph -->
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer quis lacus a dui dapibus lobortis. Nulla nec condimentum ante. Donec vel massa et elit placerat scelerisque. Suspendisse gravida lacus viverra leo placerat sagittis. Aenean ultricies turpis tellus, et convallis tellus pretium quis. Morbi ex est, auctor sed dictum ac, laoreet eu eros. Curabitur finibus sodales nulla id varius. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae;</p>
      <!-- /wp:paragraph -->
    </div>
    <!-- /wp:column -->

    <!-- wp:column {"width":"33.33%","backgroundColor":"ucGrey-100","textColor":"ucBlue-900"} -->
    <div class="wp-block-column has-uc-blue-900-color has-uc-grey-100-background-color has-text-color has-background" style="flex-basis:33.33%">
      <!-- wp:paragraph {"fontSize":"large"} -->
      <p class="has-large-font-size"><strong>Related Links</strong></p>
      <!-- /wp:paragraph -->

      <!-- wp:paragraph -->
      <p>Sidebar content...<br><a href="#">Sidebar content link</a></p>
      <!-- /wp:paragraph -->
    </div>
    <!-- /wp:column -->
  </div>
  <!-- /wp:columns -->
</div>
<!-- /wp:group -->