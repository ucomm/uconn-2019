<!-- wp:group {"align":"full","textColor":"ucBlue-900","layout":{"inherit":true}} -->
<div class="wp-block-group alignfull has-uc-blue-900-color has-text-color">
  <!-- wp:columns -->
  <div class="wp-block-columns">
    <!-- wp:column -->
    <div class="wp-block-column">
      <!-- wp:heading -->
      <h2>Heading</h2>
      <!-- /wp:heading -->

      <!-- wp:paragraph -->
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer quis lacus a dui dapibus lobortis. Nulla nec condimentum ante. Donec vel massa et elit placerat scelerisque. Suspendisse gravida lacus viverra leo placerat sagittis. Aenean ultricies turpis tellus, et convallis tellus pretium quis. Morbi ex est, auctor sed dictum ac, laoreet eu eros. Curabitur finibus sodales nulla id varius. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae;</p>
      <!-- /wp:paragraph -->
    </div>
    <!-- /wp:column -->

    <!-- wp:column -->
    <div class="wp-block-column">
      <!-- wp:cover {"overlayColor":"ucBlue-100"} -->
      <div class="wp-block-cover"><span aria-hidden="true" class="has-uc-blue-100-background-color has-background-dim-100 wp-block-cover__gradient-background has-background-dim"></span>
        <div class="wp-block-cover__inner-container">
          <!-- wp:paragraph {"align":"center","placeholder":"Write title…","textColor":"ucBlue-900","fontSize":"large"} -->
          <p class="has-text-align-center has-uc-blue-900-color has-text-color has-large-font-size">Replace with photo or other content...</p>
          <!-- /wp:paragraph -->
        </div>
      </div>
      <!-- /wp:cover -->
    </div>
    <!-- /wp:column -->
  </div>
  <!-- /wp:columns -->
</div>
<!-- /wp:group -->