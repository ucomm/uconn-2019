<?php

/**
 * 
 * Template Name: People
 * Notes: The file _must_ be called user-people.php for the ACF metaboxes to appear.
 * 
 */
add_filter('body_class', array('UConn2019\Lib\Helpers', 'add_angled_header_class'));
get_header();

$groups = get_terms([
  'taxonomy' => 'group',
  'orderby' => 'slug',
  'hide_empty' => false
]);

$personTags = get_terms([
  'taxonomy' => 'persontag',
  'orderby' => 'slug',
  'hide_empty' => false
]);

if (!isset($helpers) || !class_exists('UConn2019\Lib\Helpers')) {
  include UCONN_2019_DIR . '/lib/Helpers.php';
  $helpers = new \UConn2019\Lib\Helpers();
}
?>

<main id="main-content">
  <?php echo $helpers->get_angled_header(get_the_title()); ?>
  <section>
    <?php
    if (have_posts()) {
      while (have_posts()) {
        the_post();
    ?>
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
          <?php the_content(); ?>
        </article>
        <div class="people-container">
      <?php
      }
    }
      ?>
      <div class="people-filter-container">
        <form>
          <div class="select-wrapper">
            <div class="select-container group-select-container">
              <div>
                <label for="group-select">Role</label>
              </div>
              <div>
                <select name="group-select" id="group-select" aria-controls="people-grid">
                  <option value="all-groups">All Roles</option>
                  <?php
                  foreach ($groups as $group) {
                    echo "<option value='$group->slug'>$group->name</option>";
                  }
                  ?>
                </select>
              </div>
            </div>
            <div class="select-container group-select-container">
              <div>
                <label for="persontag-select">Department</label>
              </div>
              <div>
                <select name="persontag-select" id="persontag-select" aria-controls="people-grid">
                  <option value="all-persontags">All Departments</option>
                  <?php
                  foreach ($personTags as $tag) {
                    echo "<option value='$tag->slug'>$tag->name</option>";
                  }
                  ?>
                </select>
              </div>
            </div>
          </div>
          <div class="select-container name-select-container">
            <div>
              <label for="name-select">Name</label>
            </div>
            <div>
              <input type="text" name="name-select" id="name-select" placeholder="Sam Smith" aria-controls="people-grid">
            </div>
          </div>
        </form>
      </div>
      <?php
      get_template_part('template-parts/content', 'people');
      ?>
        </div>
  </section>
</main>
<?php

get_footer();
