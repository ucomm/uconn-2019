#!/bin/bash

COMPOSER_DISCARD_CHANGES=1 composer install --no-dev --no-interaction || exit 1

composer dump-autoload --no-dev --classmap-authoritative || exit 1