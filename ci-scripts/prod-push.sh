#!/bin/bash

# require specific override of --dry-run on tagged pushes to prod
DRY_RUN=--dry-run

if [ "$RSYNC_DRY_RUN" = "false" ]
  then DRY_RUN=""
fi

source="./"
sitepath="$COMM_PRODUCTION_SITES_DIR/$SITE_DIRECTORY/public_html"
themepath="$sitepath/wp-content/themes/$THEME_SLUG"

rsync $DRY_RUN -avzO -e ssh --chmod=ugo=rwX --no-perms \
--exclude-from "excludes.txt" \
--delete $source deploy@comm0-prd-web.its.uconn.edu:$themepath