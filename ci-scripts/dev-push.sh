#!/bin/bash

source="./"
sitepath="$COMM_STAGING_SITES_DIR/$SITE_DIRECTORY/public_html"
themepath="$sitepath/wp-content/themes/$THEME_SLUG"

rsync -avzO -e ssh --chmod=ugo=rwX --no-perms \
--exclude-from "excludes.txt" \
--delete $source staging0:$themepath