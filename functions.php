<?php

// Constants
use A11y\Menu_Walker;
use UConn2019\Lib\BlockPatterns;
use UConn2019\Lib\ScriptLoader;
use UConn2019\Lib\BeaverBuilder;
use UConn2019\Lib\Helpers;
use UConn2019\Lib\Customizer;
use UConn2019\Lib\UCPeople;

define( 'UCONN_2019_DIR', get_template_directory() );
define( 'UCONN_2019_URL', get_template_directory_uri() );

// Load

require_once( 'lib/CustomPostTypes.php' );
require_once( 'lib/ScriptLoader.php' );
require_once( 'lib/Menus.php' );
require_once( 'lib/Widgets.php' );
require_once( 'lib/FileTypes.php' );
require_once( 'lib/Helpers.php' );
require_once( 'lib/BlockPatterns.php' );
require_once( 'lib/BeaverBuilder.php' );
require_once( 'lib/Customizer.php' );
require_once( 'lib/UCPeople.php' );
require_once(UCONN_2019_DIR . '/vendor/autoload.php');

global $ucPeople;
global $helpers;

$blockPatterns = new BlockPatterns();
$assetLoader = new ScriptLoader();
$beaverBuilder = new BeaverBuilder();
$ucPeople = new UCPeople();
$helpers = new Helpers();

$blockPatterns->registerBlockPatternCategory();
$blockPatterns->registerBlockPatterns();

// Enqueue Scripts and Styles
$assetLoader->enqueueScripts();

// menus
$a11y_walker = new Menu_Walker(true);
$menus = new UConn2019\Lib\Menus($a11y_walker);

if (is_admin()) {
    $assetLoader->enqueueAdminScripts();
}

// Register Menus
add_action( 'after_setup_theme', array('UConn2019\Lib\Menus', 'register_menus') );
add_action( 'after_setup_theme', [$helpers, 'setup_theme'] );

// filter nav menu items for the footer
add_filter('nav_menu_item_title', array('UConn2019\Lib\Menus', 'filter_nav_item_title'), 4, 10);
// Ensure accurate copyright info in footer
if (!is_admin()) {
  UConn2019\Lib\Menus::copyright_handler();
  add_action('pre_get_posts', [ $helpers, 'tag_archive_manager' ]);
}

add_filter('wp_nav_menu_items', array('UConn2019\Lib\Menus', 'mobile_nav_search'), 10, 2);

// Register Custom Image Sizes and Create Readable Names for Admin
add_action( 'after_setup_theme', array('UConn2019\Lib\FileTypes', 'add_image_sizes') );
add_filter( 'image_size_names_choose', array('UConn2019\Lib\FileTypes', 'add_image_size_names') );

// Register Sidebars/Widgets
add_action( 'widgets_init', array('UConn2019\Lib\Widgets', 'register_widgets') );

// Alter File Types Allowed
add_action( 'upload_mimes', array('UConn2019\Lib\FileTypes', 'add_file_types_to_uploads') );

// Register Custom Post Types
add_action( 'init', array('UConn2019\Lib\CustomPostTypes', 'init'));

// Allow regular custom field boxes, in addition to ACF, as it is utilized by our alert plugin that needs this base functionality to not depend on ACF
add_filter('acf/settings/remove_wp_meta_box', '__return_false');

add_filter('map_meta_cap', [ $helpers, 'allowUnfilteredHTML'], 10, 3);

add_action('customize_register', [ Customizer::class, 'register' ]);

// remove prefixes from archive titles retrieved with get_the_archive_title()
add_filter('get_the_archive_title_prefix', '__return_false');

if (!is_plugin_active('uc-people-compat/uc-people-compat.php')) {
  add_action('init', [$ucPeople, 'addShortcode'], 10, 2);
}

if (is_plugin_active('bb-plugin/fl-builder.php')) {
    add_filter('fl_builder_register_settings_form', [BeaverBuilder::class, 'filterSettingsForm'], 10, 2);
    add_filter('fl_builder_module_custom_class', [BeaverBuilder::class, 'filterModuleClass'], 10, 2);
}