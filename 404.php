<?php get_header(); ?>

    <main role="main" aria-label="Content" id="main-content">
        <section>
            <article id="post-404">
                <h1><?php esc_html_e( 'Page not Found', 'uconn-2019' ); ?></h1>

                <p><a href="<?php echo esc_url( home_url() ); ?>"><?php esc_html_e( 'Return to the homepage.', 'uconn-2019' ); ?></a><p>
            </article>
        </section>
    </main>

<?php get_footer(); ?>