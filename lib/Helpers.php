<?php

namespace UConn2019\Lib;

use WP_Post;
use WP_Query;
class Helpers {
  
  /**
   * Makes sure that archive pages have fallback images in case a featured image isn't available.
   *
   * @param WP_Post $post
   * @param string $fallbackImagePath
   * @return string $html string
   */
  public function safe_featured_image(
    WP_Post $post, 
    string $fallbackImagePath = '/assets/img/uconn-fallback-square.jpg'
  ): string {
    $args = array(
      'class' => 'post-image'
    );
    $html = get_the_post_thumbnail($post, 'medium_large', $args);

    if ( !$html ) {
      $fallback = UCONN_2019_URL . $fallbackImagePath;
      $html = "<img class='post-image' src='$fallback' alt='uconn logo' />";
    }
    
    return $html;
  }
  /**
   * Get info about archive pages to handle CPTs
   *
   * @return array $data - info about the archive page.
   */
  public function get_archive_type(): array {
    $post_type = get_query_var('post_type');
    $data = [
      'post_type' => $post_type,
      'title' => 'Archive',
      'is_podcast' => false
    ];

    switch ($post_type) {
        case 'uconn360-podcast':
            $data['title'] = 'The UConn 360 Podcast';
            $data['is_podcast'] = true;
            break;
        
        default:
            break;
    }
    return $data;
  }

  /**
   * Get a re-useable template for pages that need an angled header.
   *
   * @param string $title
   * @return void
   */
  public function get_angled_header(string $title): void {
    include UCONN_2019_DIR . '/template-parts/angled-header.php';
  }


  /**
   * Add an angled-header class to the <body>
   *
   * @param array $classes
   * @return array
   */
  static public function add_angled_header_class($classes) {
    $classes['angled-header'] = 'angled-header';
    return $classes;
  }

  public function tag_archive_manager(WP_Query $query): void {
    if ($query->is_tag() && $query->is_main_query()) {
      $query->set('post_type', [
        'post', 
        'uconn360-podcast'
      ]);
    }

    if (isset($_GET['tag-filter']) && $_GET['tag-filter'] !== '') {
      $taxquery = [
        [
          'taxonomy' => 'post_tag',
          'field' => 'slug',
          'terms' => [$_GET['tag-filter']]
        ]
      ];

      $query->set('tax_query', $taxquery);
    }

    // Custom rule to make sure AZ index query grabs ALL & be alphabetical
    if ( !is_admin() && $query->is_main_query() && is_post_type_archive('uconn_azindex_entry') ) {
      $query->set('posts_per_page', -1);
      $query->set('orderby', 'title');
      $query->set('order', 'ASC');
    }

    // order person tax archive pages by ACF last_name field
    if (!is_admin() && $query->is_main_query() && is_tax([ 'group', 'persontag'] )) {
      $query->set('post_status', 'publish');
      $query->set('orderby', 'meta_value');
      $query->set('meta_key', 'last_name');
      $query->set('order', 'ASC');
      $query->set('posts_per_page', 12);
    }
  }
  public function setup_theme(): void {

    $supports = [
      'editor-styles' => [],
      'responsive-embeds' => []
    ];

    foreach ($supports as $support => $args) {
      add_theme_support($support, $args);
    }
  }

  public function allowUnfilteredHTML(array $caps, string $cap, int $userID): array {
    if ('unfiltered_html' === $cap && user_can($userID, 'editor')) {
      $caps = ['unfiltered_html'];
    }
    return $caps;
  }
}