<?php

namespace UConn2019\Lib;

class BlockPatterns {

  public function registerBlockPatternCategory() {
    add_action('init', [ $this, 'prepareBlockPatternCategory' ]);
  }

  public function registerBlockPatterns() {
    add_action('init', [ $this, 'preparePatterns' ]);
  }

  /**
   * Prepare a new block pattern category to make finding our custom patterns easier.
   *
   * @return boolean
   */
  public function prepareBlockPatternCategory(): bool
  {
    return register_block_pattern_category('uconn-2019', [
      'label' => __('UConn 2019', 'uconn-2019')
    ]);
  }

  /**
   * Prepare block patterns for registration. 
   * See here for registration attributes - https://developer.wordpress.org/block-editor/reference-guides/block-api/block-patterns/#register_block_pattern
   *
   * @return boolean
   */
  public function preparePatterns(): bool {
    $didRegister = false;
    $patterns = [
      'uconn-2019/fullWidthNoBG' => [
        'title' => __('Full Width Row with No Background', 'uconn-2019'),
        'description' => __('A full width row with columns and no background. Includes a heading, starter text, and optional area for an image/other content.', 'uconn-2019'),
        'content' => $this->getPatternContent('full-width-row-with-columns-no-bg.php'),
        'categories' => [
          'featured',
          'uconn-2019'
        ],
        'keywords' => [
          'layout',
          'full width'
        ]
      ],
      'uconn-2019/fullWidthAngleBGLtoR' => [
        'title' => __('Full Width Row with Angled Background (left to right)', 'uconn-2019'),
        'description' => __('A full width row with content that has an angled background going from left to right. Includes a heading, starter text, and optional area for an image/other content.', 'uconn-2019'),
        'content' => $this->getPatternContent('full-width-angle-ltor.php'),
        'categories' => [
          'featured',
          'uconn-2019'
        ],
        'keywords' => [
          'layout',
          'full width',
          'background image',
          'gradient',
          'angle',
          'left'
        ]
      ],
      'uconn-2019/fullWidthAngleBGRtoL' => [
        'title' => __('Full Width Row with Angled Background (right to left)', 'uconn-2019'),
        'description' => __('A full width row with content that has an angled background going from right to left. Includes a heading, starter text, and optional area for an image/other content.', 'uconn-2019'),
        'content' => $this->getPatternContent('full-width-angle-rtol.php'),
        'categories' => [
          'featured',
          'uconn-2019'
        ],
        'keywords' => [
          'layout',
          'full width',
          'background image',
          'gradient',
          'angle',
          'right'
        ]
      ],
      'uconn-2019/fullWidthBG50percentContent' => [
        'title' => __('Full Width Row with Background Image or Color and 50 percent content block', 'uconn-2019'),
        'description' => __('A full width row meant to be used just with an image or color. Includes a 50% content block', 'uconn-2019'),
        'content' => $this->getPatternContent('full-width-background-50-percent-content.php'),
        'categories' => [
          'featured',
          'uconn-2019'
        ],
        'keywords' => [
          'layout',
          'full width',
          'background image',
          'columns'
        ]
      ],
      'uconn-2019/fullWidthBGRow' => [
        'title' => __('Full Width Row with Background Image or Color', 'uconn-2019'),
        'description' => __('A full width row meant to be used just with an image or color with or without content.', 'uconn-2019'),
        'content' => $this->getPatternContent('full-width-background-image-placeholder.php'),
        'categories' => [
          'featured',
          'uconn-2019'
        ],
        'keywords' => [
          'layout',
          'full width',
          'background image'
        ]
      ],
      'uconn-2019/fullWidthRowFullWidthColumnsLeftCover' => [
        'title' => __('Full Width Row and Columns with Left Cover Image', 'uconn-2019'),
        'description' => __('A full width row with columns that take up the entire row. Cover image on the left.', 'uconn-2019'),
        'content' => $this->getPatternContent('full-width-row-full-width-columns-left-cover.php'),
        'categories' => [
          'featured',
          'uconn-2019'
        ],
        'keywords' => [
          'layout',
          'full width',
          'cover image',
          'background image',
          'left'
        ]
      ],
      'uconn-2019/fullWidthRowFullWidthColumnsRightCover' => [
        'title' => __('Full Width Row and Columns with Right Cover Image', 'uconn-2019'),
        'description' => __('A full width row with columns that take up the entire row. Cover image on the right.', 'uconn-2019'),
        'content' => $this->getPatternContent('full-width-row-full-width-columns-right-cover.php'),
        'categories' => [
          'featured',
          'uconn-2019'
        ],
        'keywords' => [
          'layout',
          'full width',
          'cover image',
          'background image',
          'right'
        ]
      ],
      'uconn-2019/contentRowWithRightSidebar' => [
        'title' => __('Content Row With Right Sidebar', 'uconn-2019'),
        'description' => __('A constrained width row that has a 60% column content area plus right hand sidebar.', 'uconn-2019'),
        'content' => $this->getPatternContent('content-row-with-right-sidebar.php'),
        'categories' => [
          'featured',
          'uconn-2019'
        ],
        'keywords' => [
          'layout',
          'sidebar',
          'columns',
          'right'
        ]
      ],
      'uconn-2019/imageWithShadowRow' => [
        'title' => __('Image with Shadow Row', 'uconn-2019'),
        'description' => __('A row that has an image with a shadow. For instance, can be used for a featured headshot.', 'uconn-2019'),
        'content' => $this->getPatternContent('image-with-shadow-row.php'),
        'categories' => [
          'featured',
          'uconn-2019'
        ],
        'keywords' => [
          'layout',
          'row',
          'headshot',
          'image',
          'columns',
          'shadow'
        ]
      ],
      'uconn-2019/coverWithRedShadow' => [
        'title' => __('Cover with Red Shadow', 'uconn-2019'),
        'description' => __('A cover block (like for an image) with a red shadow.', 'uconn-2019'),
        'content' => $this->getPatternContent('cover-with-red-shadow.php'),
        'categories' => [ 'uconn-2019' ],
        'keywords' => [
          'shadow',
          'image',
          'cover',
          'headshot',
          'featured image'
        ],
        'viewportWidth' => 500
      ],
      'uconn-2019/coverWithBlueShadow' => [
        'title' => __('Cover with Blue Shadow', 'uconn-2019'),
        'description' => __('A cover block (like for an image) with a blue shadow.', 'uconn-2019'),
        'content' => $this->getPatternContent('cover-with-blue-shadow.php'),
        'categories' => ['uconn-2019'],
        'keywords' => [
          'shadow',
          'image',
          'cover',
          'headshot',
          'featured image'
        ],
        'viewportWidth' => 500
      ],
      'uconn-2019/coverWithDarkBlueShadow' => [
        'title' => __('Cover with Dark Blue Shadow', 'uconn-2019'),
        'description' => __('A cover block (like for an image) with a dark blue shadow.', 'uconn-2019'),
        'content' => $this->getPatternContent('cover-with-dark-blue-shadow.php'),
        'categories' => ['uconn-2019'],
        'keywords' => [
          'shadow',
          'image',
          'cover',
          'headshot',
          'featured image'
        ],
        'viewportWidth' => 500
      ],
      'uconn-2019/coverWithGreyShadow' => [
        'title' => __('Cover with Grey Shadow', 'uconn-2019'),
        'description' => __('A cover block (like for an image) with a grey shadow.', 'uconn-2019'),
        'content' => $this->getPatternContent('cover-with-grey-shadow.php'),
        'categories' => ['uconn-2019'],
        'keywords' => [
          'shadow',
          'image',
          'cover',
          'headshot',
          'featured image'
        ],
        'viewportWidth' => 500
      ]
    ];

    foreach ($patterns as $name => $pattern) {
      $didRegister = register_block_pattern($name, $pattern);
    }

    return $didRegister;
  }

  /**
   * Get the pattern copied from the original block configuration in the editor
   *
   * @param string $fileName
   * @return string|false
   */
  private function getPatternContent(string $fileName) {
    ob_start();
    include UCONN_2019_DIR . '/partials/block-patterns/' . $fileName;
    return ob_get_clean();
  }
}