<?php

namespace UConn2019\Lib;

class BeaverBuilder {

  public function __construct()
  {
  }

  /**
   * Add a custom "appearance" setting to buttons
   * More easily gives people style options for modules
   *
   * @param array $form the BB settings form
   * @param string $id the module ID name
   * @return array
   */
  public static function filterSettingsForm(array $form, string $id): array
  {

    if ($id === 'button') {
      $appearance = [
        'type' => 'select',
        'label' => 'Button Appearance',
        'default' => 'default',
        'options' => [
          'default' => 'Default',
          'is-style-royal-blue-white-hover' => 'Royal Blue/White Hover',
          'is-style-white-royal-blue-hover' => 'White/Royal Blue Hover'
        ]
      ];
      $form['general']['sections']['general']['fields']['appearance'] = $appearance;
    }

    if ($id === 'heading') {
      
      $form['general']['sections']['general']['fields']['tag']['toggle'] = [
        'h2' => [
            'fields' => [ 'appearance' ]
          ]
        ];
        
      $appearance = [
        'type' => 'select',
        'label' => 'Heading Appearance',
        'default' => 'default',
        'options' => [
          '' => 'Default',
          'is-style-h2-small' => 'H2 - Small'
        ]
      ];

      $form['general']['sections']['general']['fields']['appearance'] = $appearance;
    }

    return $form;
  }

  /**
   * Adds an additional class to buttons based on their custom appearance setting
   *
   * @param string $class
   * @param object $module
   * @return string
   */
  public static function filterModuleClass(string $class, object $module): string {

    if ('FLButtonModule' === get_class($module)) {
      $appearance = $module->settings->appearance;
      $class .= ' ' . $appearance;
    }

    if ('FLHeadingModule' === get_class($module)) {
      $appearance = $module->settings->appearance;
      $class .= ' ' . $appearance;
    }

    return $class;
  }

  /**
   * NB - this currently doesn't work. it needs to be investigated why not. the filter it requiers causes a wide range of errors
   * 
   * Allow users "to enter sensitive code with tags such as <script> or <iframe> into post titles and content."
   * See - https://docs.wpbeaverbuilder.com/beaver-builder/troubleshooting/common-issues/error-settings-not-saved/#filter-examples
   *
   * @param array $config
   * @return array
   */
  public static function filterUIConfig(array $config): array {
    $user = wp_get_current_user();
    $role = $user->roles[0];
    if ('administrator' === $role || 'editor' === $role) {
      $config['userCaps'] = [
        'unfiltered_html' => true,
        'global_unfiltered_html' => true
      ];
    }
    return $config;
  }
}