<?php

namespace UConn2019\Lib;

class ScriptLoader {

    protected $buildDir;

    public function __construct()
    {   
        $this->buildDir = wp_get_environment_type() === 'local' || 
            wp_get_environment_type() === 'development' ?
        '/build' :
        '/dist'; 
    }

    public function enqueueScripts() {
        add_action('wp_enqueue_scripts', [ $this, 'load_scripts' ]);
    }

    public function enqueueAdminScripts() {

        // make additions to the theme.json settings available in the editor.
        add_editor_style($this->buildDir . '/editor-styles.css');

        add_action('admin_enqueue_scripts', [ $this, 'load_admin_scripts' ], 9999);
        add_action('enqueue_block_editor_assets', [ $this, 'enqueue_block_editor_assets'] );
    }

    public function load_scripts() {
        // Core
        wp_enqueue_style('uconn-2019-styles', UCONN_2019_URL . $this->buildDir . '/index.css');
        wp_enqueue_script('uconn-2019-script', UCONN_2019_URL . $this->buildDir . '/index.js', array('jquery', 'a11y-menu'), false, true);

        // A11y Menu
        wp_register_script('a11y-menu', UCONN_2019_URL . '/vendor/ucomm/a11y-menu/dist/Navigation.min.js', array(), false, true);
        wp_enqueue_style('a11y-menu-styles', UCONN_2019_URL . '/vendor/ucomm/a11y-menu/dist/main.css');
        wp_enqueue_script('a11y-menu');

        // People filter
        if (is_page_template('user-people.php')) {
            wp_enqueue_script('people-filter', UCONN_2019_URL . $this->buildDir . '/people-filter.js', [ 'jquery' ], false, true);
        }
    }

    public function load_admin_scripts(string $hook) {
        if ('post.php' === $hook) {
            wp_enqueue_style('editor-style-custom-values', UCONN_2019_URL . $this->buildDir . '/editor.css');
            wp_enqueue_script('editor-script', UCONN_2019_URL . $this->buildDir . '/editor.js', [], false, true);
        }
    }

    public function enqueue_block_editor_assets() {
        wp_enqueue_script(
            'block-style-overrides', 
            UCONN_2019_URL . $this->buildDir . '/block-style-overrides.js', 
            [ 'wp-blocks' ], 
            false, 
            true
        );
    }
}