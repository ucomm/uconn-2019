<?php

namespace UConn2019\Lib;

class CustomPostTypes {
  public static function init() {
    self::podcast_cpt();
  }

  /**
   * Register the Podcast CPT.
   *
   * @return void
   */
  private static function podcast_cpt() {
    $labels = array(
      'name'                  => _x( 'Podcasts', 'Post Type General Name', 'uc-podcast' ),
      'singular_name'         => _x( 'Podcast', 'Post Type Singular Name', 'uc-podcast' ),
      'menu_name'             => __( 'Podcasts', 'uc-podcast' ),
      'name_admin_bar'        => __( 'Podcast', 'uc-podcast' ),
      'archives'              => __( 'Podcast Archives', 'uc-podcast' ),
      'attributes'            => __( 'Podcast Attributes', 'uc-podcast' ),
      'parent_item_colon'     => __( 'Parent Item:', 'uc-podcast' ),
      'all_items'             => __( 'All Podcasts', 'uc-podcast' ),
      'add_new_item'          => __( 'Add New Podcast', 'uc-podcast' ),
      'add_new'               => __( 'Add New', 'uc-podcast' ),
      'new_item'              => __( 'New Podcast', 'uc-podcast' ),
      'edit_item'             => __( 'Edit Podcast', 'uc-podcast' ),
      'update_item'           => __( 'Update Podcast', 'uc-podcast' ),
      'view_item'             => __( 'View Podcast', 'uc-podcast' ),
      'view_items'            => __( 'View Podcasts', 'uc-podcast' ),
      'search_items'          => __( 'Search Podcast', 'uc-podcast' ),
      'not_found'             => __( 'No podcasts found', 'uc-podcast' ),
      'not_found_in_trash'    => __( 'No podcasts found in Trash', 'uc-podcast' ),
      'featured_image'        => __( 'Featured Image', 'uc-podcast' ),
      'set_featured_image'    => __( 'Set featured image', 'uc-podcast' ),
      'remove_featured_image' => __( 'Remove featured image', 'uc-podcast' ),
      'use_featured_image'    => __( 'Use as featured image', 'uc-podcast' ),
      'insert_into_item'      => __( 'Insert into podcast', 'uc-podcast' ),
      'uploaded_to_this_item' => __( 'Uploaded to this podcast', 'uc-podcast' ),
      'items_list'            => __( 'Podcasts list', 'uc-podcast' ),
      'items_list_navigation' => __( 'Podcasts list navigation', 'uc-podcast' ),
      'filter_items_list'     => __( 'Filter podcasts list', 'uc-podcast' ),
    );
    $args = array(
      'label'                 => __( 'Podcast', 'uc-podcast' ),
      'description'           => __( 'Podcasts', 'uc-podcast' ),
      'labels'                => $labels,
      'supports'              => array( 'title', 'editor', 'thumbnail' ),
      'taxonomies'            => array( 'category', 'post_tag' ),
      'hierarchical'          => false,
      'public'                => true,
      'show_ui'               => true,
      'show_in_menu'          => true,
      'menu_position'         => 5,
      'menu_icon'             => 'dashicons-playlist-audio',
      'show_in_admin_bar'     => true,
      'show_in_nav_menus'     => true,
      'can_export'            => true,
      'has_archive'           => true,
      'exclude_from_search'   => true,
      'publicly_queryable'    => true,
      'capability_type'       => 'page',
      'show_in_rest'          => true,
    );
	  register_post_type( 'uconn360-podcast', $args );
  }
}