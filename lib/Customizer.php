<?php

namespace UConn2019\Lib;

use WP_Customize_Manager;
use WP_Customize_Control;

class Customizer {

  public static $social = [
    [
      'link' => 'https://www.tiktok.com/@uconn?lang=en',
      'socialType' => 'Tiktok',
      'iconClass' => 'fab fa-tiktok'
    ],
    [
      'link' => 'https://www.facebook.com/UConn/',
      'socialType' => 'Facebook',
      'iconClass' => 'fab fa-facebook-f'
    ],
    [
      'link' => 'https://twitter.com/uconn',
      'socialType' => 'Twitter',
      'iconClass' => 'fab fa-x-twitter'
    ],
    [
      'link' => 'https://www.youtube.com/uconn',
      'socialType' => 'YouTube',
      'iconClass' => 'fab fa-youtube'
    ],
    [
      'link' => 'https://www.instagram.com/uconn',
      'socialType' => 'Instagram',
      'iconClass' => 'fab fa-instagram'
    ],
    [
      'link' => 'https://www.linkedin.com/school/university-of-connecticut/',
      'socialType' => 'LinkedIn',
      'iconClass' => 'fab fa-linkedin'
    ]
  ];

  public static function register(WP_Customize_Manager $wp_customize) {

    $wp_customize->add_section('uconn_social_section', [
      'title' => __('UConn Social', 'uconn-2019'),
      'priority' => 30,
      'description' => __('Update/Manage the social links in the footer of the site', 'uconn-2019')
    ]);


    foreach (self::$social as $index => $s) {
      $count = $index + 1;
      $wp_customize->add_setting('footer_social_link_' . $count, [
        'default' => $s['link'],
        'type' => 'theme_mod'
      ]);

      $wp_customize->add_setting('footer_social_link_aria_' . $count, [
        'default' => __('Visit us on ' . $s['socialType'], 'uconn-2019'),
        'type' => 'theme_mod'
      ]);

      $wp_customize->add_setting('footer_social_link_icon_' . $count, [
        'default' => $s['iconClass'],
        'type' => 'theme_mod'
      ]);


      $wp_customize->add_control(
        new WP_Customize_Control(
          $wp_customize,
          'footer_social_link_control_' . $count,
          [
            'settings' => 'footer_social_link_' . $count,
            'section' => 'uconn_social_section',
            'label' => __('Footer Social Link ' . $count, 'uconn-2019'),
            'type' => 'text'
          ]
        )
      );

      $wp_customize->add_control(
        new WP_Customize_Control(
          $wp_customize,
          'footer_social_link_aria_control_' . $count,
          [
            'settings' => 'footer_social_link_aria_' . $count,
            'section' => 'uconn_social_section',
            'label' => __('Footer Social Aria Text (Hidden)', 'uconn-2019'),
            'type' => 'text'
          ]
        )
      );

      $wp_customize->add_control(
        new WP_Customize_Control(
          $wp_customize,
          'footer_social_link_icon_control_' . $count,
          [
            'settings' => 'footer_social_link_icon_' . $count,
            'section' => 'uconn_social_section',
            'label' => __('Footer Social Icon', 'uconn-2019'),
            'type' => 'text'
          ] 
        )
      );
    }
  }
}