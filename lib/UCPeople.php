<?php

namespace UConn2019\Lib;

use WP_Query;
use stdClass;

/**
 * 
 * A class to help manage the UC People plugin
 * 
 */
class UCPeople {

  protected $isUCPeopleActive;

  public function __construct()
  {
    if (!function_exists('is_plugin_active')) {
      include ABSPATH . 'wp-admin/includes/plugin.php';
    }
    $this->isUCPeopleActive = is_plugin_active('uc-people/uc-people.php');
  }

  /**
   * Add support for the [ucomm-people] shortcode
   *
   * @return void
   */
  public function addShortcode() {
    add_shortcode('ucomm-people', [ $this, 'prepareShortcode' ]);
  }

  /**
   * Converts the uc-people template page ACF fields into a flexible shortcode.
   * 
   * @param array $atts the shortcode attributes
   * @return string the templated people
   */
  public function prepareShortcode($atts = []): string {

    $attributes = shortcode_atts([
      'break_into_groups' => false,
      'information_to_display' => 'photo, first name, last name, title, email, phone',
      'layout' => 'grid',
      'persons_per_row' => '3',
      'specific_groups' => '',
      'specific_people' => '',
      'specific_tags' => ''
    ], $atts);

    $args = [
      'break_into_groups' => filter_var($attributes['break_into_groups'], FILTER_VALIDATE_BOOLEAN),
      'layout' => $attributes['layout'],
      'persons_per_row' => $attributes['persons_per_row']
    ];

    $specificGroups = [];
    $specificTags = [];
    $specificPeopleIDs = [];
    $informationToDisplay = [];

    if ('' !== $attributes['specific_groups']) {
      $specificGroups = $this->getTaxIDsFromSlugs($attributes['specific_groups'], 'group');
    }

    if ('' !== $attributes['specific_tags']) {
      $specificTags = $this->getTaxIDsFromSlugs($attributes['specific_tags'], 'persontag');
    }

    if ('' !== $attributes['specific_people']) {
      $specificPeopleIDs = $this->getSpecificPeopleIDs($attributes['specific_people']);
    }

    if ('' !== $attributes['information_to_display']) {
      $infoTypes = explode(',', $attributes['information_to_display']);
      $informationToDisplay = array_map(function($info) {
        $trimmedType = trim($info);
        return str_replace(' ', '_', $trimmedType);
      }, $infoTypes);
    }

    ob_start();
    if (count($specificGroups) > 0) {
      $args['specific_groups'] = $specificGroups;
    }

    if (count($specificTags) > 0) {
      $args['specific_tags'] = $specificTags;
    }
    
    if (count($specificPeopleIDs) > 0) {
      $args['specific_people'] = $specificPeopleIDs;
    }

    $args['information_to_display'] = $informationToDisplay;

    echo "<div class='people-container'>";
    get_template_part('template-parts/content', 'people', $args);
    echo "</div>";

    return ob_get_clean();
  }

  /**
   * 
   * Only load the user-people.php template if the UCPeople plugin is active
   * 
   * @param array $pageTemplates the available templates
   * 
   * @returns array $pageTemplates the filtered templates
   * 
   */
  public function filterPageTemplates(array $pageTemplates): array {
    $hasPeopleTemplate = isset($pageTemplates['user-people.php']);

    if (!$this->isUCPeopleActive && $hasPeopleTemplate) {
      unset($pageTemplates['user-people.php']);
    }

    return $pageTemplates;
  }

  public function removeCustomMetaboxes(string $postType, \WP_Post $post) {
    global $wp_meta_boxes;

    $wp_meta_boxes;
    // return $wp_meta_boxes;
  }

  /**
   * Handles fetching data for a group of people and then displaying them from a partial
   * Sorts the people alphabetically by last name
   *
   * @param array $ids IDs of the people posts to fetch data on
   * @param array $infoToDisplay 
   * 
   * @return boolean
   */
  public function displayPeople(array $config): bool
  {
    $templateReturned = false;
    if (!count($config['peopleIDs'])) {
      $templateReturned = get_template_part('partials/people/layout', 'error');
      return $templateReturned !== false ? true : false;
    }

    $peopleMeta = $this->getPeopleMetaData($config['peopleIDs'], $config['infoToDisplay']);

    uasort($peopleMeta, function($a, $b) {
      return strnatcasecmp($a['last_name'], $b['last_name']);
    });

    $templateArgs = [
      'data' => $peopleMeta,
      'perRow' => $config['perRow']
    ];

    if (isset($config['taxSlug'])) {
      $templateArgs['taxSlug'] = $config['taxSlug'];
    }

    $templateReturned = get_template_part('partials/people/layout', $config['layout'], $templateArgs);

    return $templateReturned !== false ? true : false;
  }

  public function displayTaxInfo(object $taxInfo): bool
  {
    $templateReturned = get_template_part('partials/people/tax', 'info', [
      'name' => $taxInfo->taxName,
      'slug' => $taxInfo->taxSlug
    ]);

    return $templateReturned !== false ? true : false;
  }

  /**
   * Break ACF fields on a semi-colon delimiter
   *
   * @param string $field
   * @return string
   */
  public function breakField(string $field): string {
    // ACF doesn't always handle &'s correctly
    $replacedAmpersand = str_replace('&amp;', '&', $field);
    $hasSemicolon = strpos($replacedAmpersand, ';');
    if (false !== $hasSemicolon) {
      $fieldParts = explode(';', trim($replacedAmpersand));
      $field = implode('<br />', $fieldParts);
    }
    return $field;
  } 
  /**
   * Associates a group of People IDs with a particular taxonomy
   *
   * @param array $taxIDs
   * @param string $taxSlug
   * @return array
   */
  public function getTaxonomyInfo(string $taxSlug, array $taxIDs = []): array {

    $filteredIDs = array_filter($taxIDs, function($val) {
      if (null !== $val) {
        return $val;
      }
    });

    return array_map(function($taxID) use ($taxSlug) {
      $term = get_term($taxID);
      $people = get_posts([
        'posts_per_page' => -1,
        'post_type' => 'person',
        'post_status' => 'publish',
        'fields' => 'ids',
        'tax_query' => [
          [
            'taxonomy' => $taxSlug,
            'field' => 'term_id',
            'terms' => $term
          ]
        ]
      ]);

      $taxInfo = new stdClass();
      $taxInfo->taxID = $taxID;
      $taxInfo->taxName = $term->name;
      $taxInfo->taxSlug = $term->slug;
      $taxInfo->peopleIDs = $people;
      
      return $taxInfo;

    }, $filteredIDs);
  }

  /**
   * Creates an array of data from ACF for each person. Items that are not in `$infoToDisplay` are set to `null` for easier validation
   *
   * @param array $peopleIDs
   * @param array $infoToDisplay
   * @return array
   */
  public function getPeopleMetaData(array $peopleIDs, array $infoToDisplay): array {
    return array_map(function($ID) use ($infoToDisplay) {
      $info = [
        'id' => $ID
      ];
      $attributes = get_fields($ID);
      foreach ($attributes as $key => $value) {
        $info[$key] = array_search($key, $infoToDisplay) !== false ? $value : null;
      }
      $info['use_photo'] = array_search('photo', $infoToDisplay) !== false ? true : false;
      $info['phone_alternate'] = $attributes['phone_(alternate)'] ?? '';
      return $info;
    }, $peopleIDs);    
  }

  /**
   * Gets and normalizes info about a particular taxonomy term for a person
   *
   * @param integer $ID the post ID of the current person
   * @param string $taxonomy the taxonomy to fetch info for
   * @return array
   */
  public function getPersonTermInfo(int $ID, string $taxonomy): array {
    $personTerms = get_the_terms($ID, $taxonomy);

    if (!$personTerms || is_wp_error($personTerms)) {
      return [];
    }

    return array_map(function ($term) {
      $link = get_term_link($term->term_id);
      $hasLink = is_wp_error($link) ? false : true;
      return [
        'value' => $term->name,
        'link' => $link,
        'hasLink' => $hasLink
      ];
    }, $personTerms);
  }

  /**
   * Construct a name based on a person post's meta data
   *
   * @param array $data
   * @return string
   */
  public function getFullName(array $data): string {
    $fullName = '';

    $firstName = $data['first_name'] ?? $data['first_name'];
    $lastName = $data['last_name'] ?? $data['last_name'];
    $middleName = $data['middle_name'] ?? $data['middle_name'];

    if ($firstName) {
      $fullName .= $firstName . ' ';
    }

    if ($middleName) {
      $fullName .= $middleName . ' ';
    }

    if ($lastName) {
      $fullName .= $lastName;
    }

    return $fullName;
  }

  /**
   * Query person ids for taxonomy pages
   *
   * @param string $taxonomy
   * @return array
   */
  public function getPeopleIDsByTaxonomy(string $taxonomy): array {
    $ids = [];
    $paged = get_query_var('paged') ? get_query_var('paged') : 1;
    $idQuery = new WP_Query([
      'post_type' => 'person',
      'post_status' => 'publish',
      'posts_per_page' => 12,
      'paged' => $paged,
      'fields' => 'ids',
      'tax_query' => [
        [
          'taxonomy' => $taxonomy,
          'terms' => get_queried_object_id(),
          'field' => 'term_id'
        ]
      ],
      'meta_key' => 'last_name',
      'orderby' => 'meta_value',
      'order' => 'ASC'
    ]);
    if ($idQuery->have_posts()) {
      $ids = $idQuery->posts;
    }
    wp_reset_query();
    return $ids;
  }

  private function getTaxIDsFromSlugs(string $slugs, string $taxonomy) {
    if (!taxonomy_exists($taxonomy)) {
      return;
    }
    $taxSlugs = explode(',', $slugs);
    return array_map(function ($slug) use ($taxonomy) {
      $trimmedSlug = trim($slug);
      $groupTax = get_term_by('slug', $trimmedSlug, $taxonomy);
      if ($groupTax) {
        return $groupTax->term_id;
      }
    }, $taxSlugs);
  }

  private function getSpecificPeopleIDs(string $names) {
    $nameArray = explode(', ', $names);

    return array_map(function($name) {
      $person = get_page_by_title($name, 'OBJECT', 'person');
      return $person->ID;
    }, $nameArray);
  }
}