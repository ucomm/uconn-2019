<?php

namespace UConn2019\Lib;

class Forminator {
  /**
   * Annoyingly, Forminator doesn't let editors see anything having to do with forms by default.
   * Why? I have no idea. It's been a ticket with them since 2018...
   *
   * @param string $cap
   * @return string
   */
  public static function filterAdminCap(string $cap): string {
    $cap = 'delete_others_posts';
    return $cap;
  }
}