<?php

namespace UConn2019\Lib;

/**
 * A class for handling functions related to specific file types.
 *  - svg
 *  - images
 */
class FileTypes {

    /**
     * Filter the types of files allowed to be uploaded
     *
     * @param array $file_types
     * @return array
     */
    public static function add_file_types_to_uploads($file_types) {
        $new_filetypes = array();

        // SVG
        $new_filetypes['svg'] = 'image/svg+xml';

        $file_types = array_merge($file_types, $new_filetypes);
        return $file_types;
    }


    /**
     * Ensure theme support for thumbnails and custom image sizes.
     *
     * @return void
     */
    public static function add_image_sizes() {
        add_theme_support('post-thumbnails');
        self::create_image_sizes();
    }

    /**
     * Filter the list of default image sizes.
     * Add custom sizes to admin/page builder areas.
     *
     * @param array $sizes
     * @return array
     */
    public static function add_image_size_names($sizes) {
        $readable_names = array();
        $image_sizes = self::get_image_sizes();

        foreach ($image_sizes as $slug => $size) {
            $readable_names[$slug] = $size['readable_name'];
        }

        return array_merge($sizes, $readable_names);
    }

    /**
     * Create custom image sizes from an array.
     *
     * @return void
     */
    private static function create_image_sizes() {
        $image_sizes = self::get_image_sizes();

        foreach ($image_sizes as $name => $size) {
            add_image_size($name, $size['width'], $size['height']);
        }
    }

    /**
     * A function to keep custom image sizes organized.
     *
     * @return array
     */
    private static function get_image_sizes() {
        return array(
            'fb-twitter-share' => array(
                'readable_name' => __('FB/Twitter Share 1.9:1', 'uconn-2019'),
                'width' => 1200,
                'height' => 630
            ),
            'instagram-share' => array(
                'readable_name' => __('Instagram Share 1:1', 'uconn-2019'),
                'width' => 1080,
                'height' => 1080
            ),
            'linkedin-share' => array(
                'readable_name' => __('LinkedIn Share 1.4:1', 'uconn-2019'),
                'width' => 1200,
                'height' => 857
            ),
            'snapchat-share' => array(
                'readable_name' => __('Snapchat Share 9:16', 'uconn-2019'),
                'width' => 1080,
                'height' => 1920
            )
        );
    }
}