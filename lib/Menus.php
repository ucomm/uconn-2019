<?php

namespace UConn2019\Lib;

use A11y\Menu_Walker;

class Menus {

    protected static $walker;

    public function __construct(Menu_Walker $walker) {
        self::$walker = $walker;
    }

    // Catch All
    public static function register_menus() {
        self::register_top_navigation();
        self::register_mobile_navigation();
        self::register_footer_navigation();
        self::register_bottom_footer_navigation();
        self::register_podcast_navigation();
    }

    // Individual Menus Registered
    public static function register_top_navigation() {
        register_nav_menu( 'main', __('Main Navigation', 'uconn-2019-theme') );
    }
    // Helper to display the top menu
    public static function display_top_navigation() {
        $walker = self::$walker;
        wp_nav_menu( array(
            'theme_location' => 'main',
            'container' => 'nav',
            'container_id' => 'am-main-nav',
            'menu_id' => 'am-main-menu',
            'items_wrap' => '<ul id="%1$s" class="am-click-menu %2$s">%3$s</ul>',
            // 'walker' => $walker
            'walker' => $walker
        ) );
    }

    public static function register_mobile_navigation() {
        register_nav_menu( 'mobile-menu', __('Mobile Navigation', 'uconn-2019-theme') );
    }

    public static function display_mobile_navigation() {
        wp_nav_menu( array( 'theme_location' => 'mobile-menu' ) );
    }

    /**
     * Add the A-Z index and search links to the mobile menu
     *
     * @param string $items
     * @param object $args
     * @return void
     */
    public static function mobile_nav_search($items, $args) {
        if ($args->theme_location === 'mobile-menu') {
            $search_items = "
            <li>
                <a href='/az'>A-Z Index</a>
            </li>
            <li>
                <a href='/search'>Search</a>
            </li>
            ";
            $items = $search_items . $items;
        }
        return $items;
    }

    public static function register_footer_navigation() {
        register_nav_menu( 'footer-one', __('Footer One Navigation', 'uconn-2019-theme') );
        register_nav_menu( 'footer-two', __('Footer Two Navigation', 'uconn-2019-theme') );
        register_nav_menu( 'footer-three', __('Footer Three Navigation', 'uconn-2019-theme') );
    }
    public static function display_footer_one_navigation() {
        wp_nav_menu( array( 'theme_location' => 'footer-one' ) );
    }
    public static function display_footer_two_navigation() {
        wp_nav_menu( array( 'theme_location' => 'footer-two' ) );
    }
    public static function display_footer_three_navigation() {
        wp_nav_menu( array( 'theme_location' => 'footer-three' ) );
    }


    public static function register_bottom_footer_navigation() {
        register_nav_menu( 'bottom-footer', __('Bottom Footer Navigation', 'uconn-2019-theme') );
    }
    public static function display_bottom_footer_navigation() {
        wp_nav_menu( array( 'theme_location' => 'bottom-footer' ) );
    }

    public static function register_podcast_navigation() {
        register_nav_menu('podcast-social-menu', __('Podcast Social Menu Twitter, facebook, etc)', 'uconn-2019-theme'));
        register_nav_menu('podcast-subscription-menu', __('Podcast Subscription Menu, (e.g. itunes, google play, etc)', 'uconn-2019-theme'));
    }

    public static function display_podcast_navigation() {
        $subscription_args = array(
            'theme_location' => 'podcast-subscription-menu',
            'menu_class' => 'podcast-social-list',
            'container' => false,
            'items_wrap' => '<ul id="%1$s" class="%2$s"><span class="podcast-social-text">Subscribe on:</span>%3$s</ul>'
        );
        wp_nav_menu($subscription_args);

        $social_args = array(
            'theme_location' => 'podcast-social-menu',
            'menu_class' => 'podcast-social-list',
            'container' => false,
            'items_wrap' => '<ul id="%1$s" class="%2$s"><span class="podcast-social-text">Follow us on:</span>%3$s</ul>'
        );
        wp_nav_menu($social_args);
    }


    /**
     * Ensure that if copyright text exists in the menu that the date stays current.
     *
     * @param string $title
     * @param object $item
     * @param array $args
     * @param int $depth
     * @return string
     */
    static public function filter_nav_item_title($title, $item, $args, $depth) {
        $text = "university of connecticut";

        // only filter if the item matches.
        if ($args->menu->slug !== 'bottom-footer-menu' || stripos($title, $text) === false) {
            return $title;
        }

        $cr_symbol  = "©";
        $current_year = date('Y');

        $cr_position = stripos($title, $cr_symbol);
        $text_position = stripos($title, $text);
        
        if ($text_position !== 0 && preg_match('/[©A-Za-z0-9 ]/', $title)) {
        
            $parts = explode(' ', $title);

            // check for the © symbol and the correct year.
            if ($parts[0] !== $cr_symbol && $parts[0] !== $current_year) {

                //  change the year
                $parts[0] = $current_year;

                // put the symbol at the beginning of the array
                array_unshift($parts, $cr_symbol);

            } elseif ($parts[0] === $cr_symbol && $parts[1] !== $current_year) {
                $parts[1] = $current_year;
            }

            $title = implode(' ', $parts);

        } elseif ($text_position === 0) {
            $title = $cr_symbol . " " . $current_year . " " . $title;
        }

        return $title;
    }
    /*
     * Ensure that the last item in the menu will be the copyright info.
     *
     * @param string $menu - the slug of the menu to check
     * @return bool/string - returns false if the menu has copyright info. otherwise add the info to the list of menu items.
     */
    static public function copyright_handler() {
        $text = "university of connecticut";

        $theme_locations = get_nav_menu_locations();

        if ( isset($theme_locations['bottom-footer']) ) {
            $footer_location = get_term($theme_locations['bottom-footer'], 'nav_menu');
            $slug = $footer_location->slug;

            $menu_items = wp_get_nav_menu_items($slug);

            $last_item = $menu_items[count($menu_items) - 1];

            if (stripos($last_item->post_title, $text) === false) {
                // create a new menu item by filtering the menu slug.
                $filter_name = 'wp_nav_menu_' . $slug . '_items';

                add_filter($filter_name, function($items) {
                    // create copyright text and list item.
                    $cr_text = '© ' . date('Y') . ' University of Connecticut';
                    $cr_item = '<li class="menu-item menu-item-type-custom"><a>' . $cr_text . '</a></li>';

                    // add to the $items string
                    $items .= $cr_item;

                    return $items;
                });
            } 
        }
        
        return false;
    }
}