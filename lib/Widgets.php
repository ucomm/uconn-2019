<?php

namespace UConn2019\Lib;

class Widgets {

    // Catch All
    public static function register_widgets() {
        self::register_footer_widgets();
    }

    public static function register_footer_widgets() {
        $widget_areas = array(
            'uc_footer_left' => 'Footer Left',
            'uc_footer_middle' => 'Footer Middle',
            'uc_footer_right' => 'Footer Right',
            'uc_footer_bottom_left' => 'Bottom Footer Left',
            'uc_footer_bottom_right' => 'Bottom Footer Right'
        );

        foreach ( $widget_areas as $slug => $name ) {
            register_sidebar( array(
                'name'  => $name,
                'id'    => $slug,
                'before_widget' => '<div>',
                'after_widget'  => '</div>'
            ) );
        }
    }

}