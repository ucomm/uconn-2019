const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
const ForkTsCheckerNotifierWebpackPlugin = require('fork-ts-checker-notifier-webpack-plugin');
const DependencyExtractionWebpackPlugin = require('@wordpress/dependency-extraction-webpack-plugin');

const env = process.env.NODE_ENV
const mode = env !== 'production' ? 'development' : 'production'
const buildDir = env !== 'production' ? 'build' : 'dist'

const optimization = {}

if (env === 'production') {
    optimization.minimize = true
    optimization.minimizer = [
        new CssMinimizerPlugin(),
        '...'
    ]
}

module.exports = {
    entry: {
        index: path.resolve(__dirname, 'src', 'js', 'index.ts'),
        'people-filter': path.resolve(__dirname, 'src', 'js', 'people-filter.ts'),
        'editor-styles': path.resolve(__dirname, 'src', 'admin', 'editor', 'styles.scss'),
        editor: path.resolve(__dirname, 'src', 'admin', 'editor', 'editor.ts'),
        'block-style-overrides': path.resolve(__dirname, 'src', 'admin', 'editor', 'block-styles.ts'),
        // 'admin-variables': path.resolve(__dirname, 'src', 'admin', 'editor', 'admin-variables.scss')
    },
    output: {
        path: path.resolve(__dirname, buildDir),
        filename: '[name].js',
        chunkFilename: '[name][ext]',
    },
    mode,
    optimization,
    devtool: env === 'production' ? false : 'source-map',
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: [
                            '@babel/preset-typescript'
                            [
                                '@babel/preset-env',
                                {
                                    debug: true,
                                    useBuiltIns: 'usage',
                                    corejs: 3.38
                                }
                            ],
                            [
                                '@babel/preset-react',
                                {
                                    runtime: 'automatic'
                                }
                            ]
                        ]
                    }
                }
            },
            {
                test: /\.tsx?$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'ts-loader',
                    }
                ]
            },
            {
                test: /\.s?css$/i,
                use: [
                    MiniCssExtractPlugin.loader, 
                    { 
                        loader: 'css-loader', 
                        options: { 
                            sourceMap: true,
                            importLoaders: 2,
                        } 
                    },
                    { 
                        loader: 'sass-loader', 
                        options: { 
                            sourceMap: true,
                            sassOptions: {
                                includePaths: [
                                    'node_modules/scss/**/*.scss'
                                ]
                            }
                        } 
                    }
                ]
            }
        ]
    },
    resolve: {
        extensions: ['.css', '.js', '.json', '.scss', '.ts', '.tsx', '.jsx']
    },
    externals: {
        jquery: 'jQuery',
        navigation: 'Navigation'
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: '[name].css',
        }),
        new ForkTsCheckerNotifierWebpackPlugin(),
        new DependencyExtractionWebpackPlugin({
            injectPolyfill: true,
            useDefaults: true,
        })
    ]
}